void main (void)
{
  vec4 color = screenf_get_original_color();
  gl_FragColor = vec4(0.0, 0.0, 0.0, color.a * (1.0 - color.x));
}
