# Piratess in Distress

Once upon a time ruthless pirates ruled the seven seas. But the time has changed. As the Admiral of Royal Navy it's your assignment to make the sea safe again. Battle, capture and tame piratesses to increase your rank in the ultimate pursue to track down and bring to justice notorious Sabre D'Orage.

## This project is abandoned

This means it is "released" almost as-is, without proper art, tutorial, balancing, several game modes, game saving, etc. But on the bright side we have a lot of bugs.

Reason: it needs too much art, at least 300-400 hours of drawing. And as I'm a bad painter I'd rather focus on gameplay-oriented game with less demand for graphics quantity and quality, and therefore this project is unlikely to receive any further development... Unless someone with enough skill and dedication decides to contribute, it's a free and open-source project after all ;)

## Content warning

This game contains material suitable only for mature audience. You can play this game only if you are a legal adult.

## About the game

Piratess in Distress is a "strip version" of SeaBattle (aka BattleShip) game with a bit different than classic set of rules, also adding additional ways to diversify the encounters with enemies also to make the game more skill-based, than luck-based.

The goal of the game is to find enemy ships on a rectangular field by shooting at specific tile and destroy opponent's ships faster than the piratess destroys yours. Ships are linear, occupy one or more tiles on the map and cannot touch each other except by corners.

Each of opponent's ship is liked to one article of piratess' clothes. Destroying all large ships allows you to use rope to tie up the enemy to prevent her from attacking and escaping. If the battle is won with the opponent tied up, the piratess is captured.

You can "tame" the captured piratess in the lower deck (prison). After tamed, the prisoner provides a persistent rank bonus that increases the size of your field, amount of your ships and in turn allows you to win against stronger opponents. Your rank is determined by all tamed piratesses in lower deck and is limited by the highest rank of the prisoners.

Some opponents have quirks that alter their behavior making fighting them harder or easier.

As you progress through ranks and defeat bosses, you unlock stronger enemies and more different props to counter their quirks.

## Installation

### Windows

No installation required - just extract the game into one folder and play.

### Linux

No installation required - just extract the game into one folder and play.

Sometimes you'll also need to set "executable" flag for the binary (`chmod +x piratess-in-distress` or through file properties in the file manager).

Make sure you have the following libraries installed to play the game:

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)

Alternatively on Debian-based systems like Ubuntu you can download DEB package and install it through a package manager or by `dpkg -i piratess-in-distress-*-linux-x86_64.deb` command.

### Android

Download and install the APK. The game requires Android 4.2 and higher.

Note that you may need to allow installation of third-party APKs in phone/tablet Settings.

## Credits

Game by EugeneLoza

Created in Castle Game Engine (https://castle-engine.io/).

Additional graphics by Petr Kratochvil, Lorc, Delapouite, Sbed.

Fonts by Rafael Souza, Bruno de Souza Leo, Nate Piekos

Sound by Kinoton, Kodack, Deathscyp, Bertsz, petenice, D.jones, bananplyte, altfuture, HarpyHarpHarp, reg7783, Kenney

Music by Nene and Centurion of War

## Links

Download the latest version: https://decoherence.itch.io/piratess-in-distress

Source code: https://gitlab.com/EugeneLoza/piratess-in-distress (GPLv3)
