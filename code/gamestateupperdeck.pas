{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameStateUpperDeck;

{$include gamecompilerconfig.inc}

interface

uses
  Classes,
  CastleVectors, CastleUIState, CastleUIControls, CastleControls, CastleKeysMouse,
  GameStateBase, GameGenerics;

type
  TUpperDeckTab = (udWild, udCaptured); // udProps?

type
  TStateUpperDeck = class(TStateBase)
  strict private
    { We don't bother saving LastTrack - it makes sense only in this run }
    LastTrack: String;
    { Contains the "Statistics" of which music track the Player has played
      Should not persist with the savegame - statistics resets every run }
    MusicDictionary: TIntegerDictionary;
    LabelPlayerRankValue: TCastleLabel;
    ActressContainer: TCastleVerticalGroup;
    ScrollViewPiratess, ScrollViewCheatSheet: TCastleUserInterface;
    procedure ClickActress(Sender: TObject);
    procedure ClickCheatSheet(Sender: TObject);
    procedure ClickLowerDeck(Sender: TObject);
    procedure ClickSevenSeas(Sender: TObject);
    procedure FillInActresses;
  public
    CurrentTab: TUpperDeckTab;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Start; override;
  end;

var
  StateUpperDeck: TStateUpperDeck;

implementation
uses
  SysUtils,
  CastleApplicationProperties, CastleWindow, CastleSoundEngine, CastleLog,
  GameGlobal, GameSounds, GameActress, GameFlagship,
  GameStateMain, GamePiratessButton;

constructor TStateUpperDeck.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/upperdeck.castle-user-interface';
  DesignPreload := true;
  MusicDictionary := ConstructIntegerDictionary(MusicsShip);
  if MusicDictionary.ContainsKey('menu_music') then
    MusicDictionary['menu_music'] := 1
  else
    WriteLnLog('"menu_music" track not found in ship music');
  LastTrack := '';
end;

destructor TStateUpperDeck.Destroy;
begin
  MusicDictionary.Free;
  inherited;
end;

procedure TStateUpperDeck.Start;
var
  Actress: TActress;
  MusicTrack: String;
begin
  inherited;

  MusicTrack := GetStringWithMinimalScore(MusicDictionary, LastTrack);
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName(MusicTrack);
  MusicDictionary[MusicTrack] := MusicDictionary[MusicTrack] + 1;
  LastTrack := MusicTrack;

  ActressContainer := DesignedComponent('ActressContainer') as TCastleVerticalGroup;
  LabelPlayerRankValue := DesignedComponent('LabelPlayerRankValue') as TCastleLabel;

  ScrollViewPiratess := DesignedComponent('ScrollViewPiratess') as TCastleUserInterface;
  ScrollViewCheatSheet := DesignedComponent('ScrollViewCheatSheet') as TCastleUserInterface;

  (DesignedComponent('ButtonLowerDeck') as TCastleButton).OnClick := @ClickLowerDeck;
  (DesignedComponent('ButtonCheatSheet') as TCastleButton).OnClick  := @ClickCheatSheet;
  (DesignedComponent('ButtonSevenSeas') as TCastleButton).OnClick := @ClickSevenSeas;

  {temporary - to have at least one wild}
  if Flagship.Tiers.Tiers[1].Unlocked then
  begin
    Actress := TActress.Create;
    Actress.Init('', Flagship.Tiers.GetRandomPiratessRank);
    Flagship.ActressesDictionary.Add(Actress.Id, Actress);
  end;

  //CurrentTab := udWild; // TODO: should be externally set or keep the previous value
  FillInActresses;
  ScrollViewPiratess.Exists := true;
  ScrollViewCheatSheet.Exists := false;
end;

procedure TStateUpperDeck.FillInActresses;
var
  A: TActress;
  B: TPiratessButton;
  List: TActressesList;
begin
  ActressContainer.ClearControls();
  List := TActressesList.Create(false);
  for A in Flagship.ActressesDictionary.Values do
    if A.Captured = (CurrentTab = udCaptured) then
      List.Add(A);
  if CurrentTab = udWild then
    List.Sort(ComparerSevenSeas)
  else
  if CurrentTab = udCaptured then
    List.Sort(ComparerLowerDeck);
  for A in List do
  begin
    B := TPiratessButton.Create(ActressContainer);
    B.Init(A);
    B.OnClick := @ClickActress;
    ActressContainer.InsertFront(B);
  end;
  List.Free;
  LabelPlayerRankValue.Caption := FloatToStringRounded(Flagship.PlayerRank, 1) + ' (' + Flagship.PlayerPhysicalRank.ToString + ')';
end;

procedure TStateUpperDeck.ClickActress(Sender: TObject);
var
  ThisActress: TActress;
  NewActress: TActress;
  OldPlayerRank: Single;
begin
  SoundClickForward;

  ThisActress := (Sender as TPiratessButton).Actress;
  // go to actress
  if CurrentTab = udWild then
  begin
    if ThisActress.Captured then
      raise Exception.Create('Cannot attack piratess, she is already captured');
    StateMain.Actress := ThisActress;
    TUiState.Current := StateMain;
  end else
  if CurrentTab = udCaptured then
  begin
    if not ThisActress.Captured then
      raise Exception.Create('Cannot interrogate piratess, she is not captured');
    //StateLowerDeck.Actress = ThisActress;
    //TUiState.Current := StateLowerDeck;
    //temporary
    if ThisActress.Tamed then
      Exit;
    if ThisActress.HasQuirk('Submissive') or (Rnd.Random < 0.1) then
    begin
      OldPlayerRank := Flagship.PlayerRank;
      ThisActress.Tamed := true;
      if ThisActress.UnlocksTier > 0 then
      begin
        if ThisActress.UnlocksTier <= 4 then
        begin
          Flagship.Tiers.Tiers[ThisActress.UnlocksTier].Unlocked := true;
          NewActress := TActress.Create;
          NewActress.Init('', Flagship.Tiers.Tiers[ThisActress.UnlocksTier].GetRandomRank);
          Flagship.ActressesDictionary.Add(NewActress.Id, NewActress);
          NewActress := TActress.Create;
          NewActress.Init('', Flagship.Tiers.Tiers[ThisActress.UnlocksTier].GetRandomRank);
          Flagship.ActressesDictionary.Add(NewActress.Id, NewActress);
          NewActress := TActress.Create;
          NewActress.Init('', Flagship.Tiers.Tiers[ThisActress.UnlocksTier].GetRandomRank);
          Flagship.ActressesDictionary.Add(NewActress.Id, NewActress);
        end else
        if ThisActress.UnlocksTier = 99 then
        begin
          NewActress := TActress.Create;
          NewActress.Init('Sabre', 169, 999);
          NewActress.CombatTheme := 'sabre_battle';
          NewActress.InterrogationTheme := 'sabre_battle'; //TODO
          NewActress.Quirks.Clear;
          NewActress.AddQuirk('Unbeatable', 1);
          NewActress.AddQuirk('Unyielding', 1);
          NewActress.AddQuirk('Fierce', 1);
          Flagship.ActressesDictionary.Add(NewActress.Id, NewActress);
        end else
        if ThisActress.UnlocksTier = 999 then
        begin
          // Win the game
        end else
          raise Exception.Create('Unexpected UnlocksTier value :' + ThisActress.UnlocksTier.ToString);
      end;
      if (OldPlayerRank < Flagship.Tiers.MaxUnlockedTier.AverageRank) and
        (Flagship.PlayerRank >= Flagship.Tiers.MaxUnlockedTier.AverageRank)
        then
      begin
        NewActress := TActress.Create;
        case Flagship.Tiers.MaxUnlockedTier.Tier of
          1: begin
               NewActress.Init('Insecte', 42, 2);
               NewActress.CombatTheme := 'insecte_battle';
               NewActress.InterrogationTheme := 'insecte_battle';
               NewActress.Quirks.Clear;
               NewActress.AddQuirk('Ambidext', 1);
             end;
          2: begin
               NewActress.Init('Dispaure', 64, 3);
               NewActress.CombatTheme := 'dispaure_battle';
               NewActress.InterrogationTheme := 'dispaure_interrogation';
               NewActress.Quirks.Clear;
               NewActress.AddQuirk('Glitchy', 1);
               NewActress.AddQuirk('Random', 1);
             end;
          3: begin
               NewActress.Init('Syrene', 100, 4);
               NewActress.CombatTheme := 'syrene_battle';
               NewActress.InterrogationTheme := 'syrene_interrogation';
               NewActress.Quirks.Clear;
               NewActress.AddQuirk('Tempting', 1);
               NewActress.AddQuirk('Shrimped', 2); // Needs this one to have more space for Player's ships
             end;
          4: begin
               NewActress.Init('Renarde', 121, 99);
               NewActress.CombatTheme := 'renarde_battle';
               NewActress.InterrogationTheme := 'renarde_interrogation';
               NewActress.Quirks.Clear;
               NewActress.AddQuirk('Clairvoyant', 1);
               NewActress.AddQuirk('Enchanting', 1);
               //Smart
             end;
        else
          raise Exception.Create('Unexpected Flagship.Tiers.MaxUnlockedTier.Tier :' + Flagship.Tiers.MaxUnlockedTier.Tier.ToString);
        end;
        Flagship.ActressesDictionary.Add(NewActress.Id, NewActress);
      end;
      FillInActresses;
    end;
  end else
    raise Exception.Create('Tried to click a piratess, but in a wrong tab');
end;

procedure TStateUpperDeck.ClickCheatSheet(Sender: TObject);
begin
  SoundClickForward;
  ScrollViewPiratess.Exists := false;
  ScrollViewCheatSheet.Exists := true;
end;

procedure TStateUpperDeck.ClickLowerDeck(Sender: TObject);
begin
  SoundClickForward;
  CurrentTab := udCaptured;
  FillInActresses;
  ScrollViewPiratess.Exists := true;
  ScrollViewCheatSheet.Exists := false;
end;

procedure TStateUpperDeck.ClickSevenSeas(Sender: TObject);
begin
  SoundClickForward;
  CurrentTab := udWild;
  FillInActresses;
  ScrollViewPiratess.Exists := true;
  ScrollViewCheatSheet.Exists := false;
end;

end.
