{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameStateContentWarning;

{$include gamecompilerconfig.inc}

interface

uses Classes,
  GameStateBase, GameGlobal;

type
  TStateContentWarning = class(TStateBase)
  strict private
    procedure ClickButtonQuit(Sender: TObject);
    procedure ClickButtonPlay(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateContentWarning: TStateContentWarning;

implementation
uses
  CastleControls, CastleUiState, CastleWindow, CastleConfig,
  GameSounds, GameStateMainMenu;

constructor TStateContentWarning.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/contentwarning.castle-user-interface';
  DesignPreload := false;
end;

procedure TStateContentWarning.Start;
begin
  inherited;
  (DesignedComponent('ButtonQuit') as TCastleButton).OnClick := @ClickButtonQuit;
  (DesignedComponent('ButtonPlay') as TCastleButton).OnClick := @ClickButtonPlay;
  (DesignedComponent('DoNotAskAgainCheckbox') as TCastleCheckBox).Checked := not UserConfig.GetValue('alwaysaskcontentwarning', false);
end;

procedure TStateContentWarning.ClickButtonQuit(Sender: TObject);
begin
  SoundClickBack;
  Application.MainWindow.Close;
end;

procedure TStateContentWarning.ClickButtonPlay(Sender: TObject);
begin
  SoundClickForward;
  if (DesignedComponent('DoNotAskAgainCheckbox') as TCastleCheckBox).Checked then
    UserConfig.SetValue('skipcontentwarning', true)
  else
    UserConfig.SetValue('alwaysaskcontentwarning', true);
  UserConfig.Save;
  TUIState.Current := StateMainMenu;
end;

end.

