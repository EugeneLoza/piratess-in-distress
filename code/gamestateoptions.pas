{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameStateOptions;

{$include gamecompilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls, CastleControls,
  GameStateBase, GameGlobal;

type
  TStateOptions = class(TStateBase)
  strict private
    procedure ClickReturn(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateOptions: TStateOptions;

implementation
uses
  SysUtils,
  CastleComponentSerialize, CastleLog, CastleUiState, CastleConfig,
  GameSounds,
  GameStateMainMenu;

constructor TStateOptions.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/options.castle-user-interface';
  DesignPreload := false;
end;

procedure TStateOptions.Start;
begin
  inherited;
  (DesignedComponent('ButtonBack') as TCastleButton).OnClick := @ClickReturn;
  {
  UserConfig.GetValue('fullscreen', false)
  UserConfig.GetFloat('volume', 1.0);
  UserConfig.GetFloat('music', 1.0);
  UserConfig.GetValue('alwaysaskcontentwarning', false)
  }
end;

procedure TStateOptions.ClickReturn(Sender: TObject);
begin
  TUiState.Pop(Self);
end;

end.
