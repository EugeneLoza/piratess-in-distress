{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameTiers;

{$include gamecompilerconfig.inc}

interface

uses
  Generics.Collections,
  GameGlobal;

type
  TTier = class(TObject)
    Tier: Integer;
    Unlocked: Boolean;
    MinRank: Single;
    MaxRank: Single;
    function GetRandomRank: Single;
    function AverageRank: Single;
    constructor Create(const ATier: Integer; const AMinRank, AMaxRank: Integer);
  end;
  TTiersDictionary = specialize TObjectDictionary<Integer, TTier>;

type
  TTiers = class(TObject)
  strict private
  public
    Tiers: TTiersDictionary;
    procedure GenerateTiers;
    function MaxUnlockedTier: TTier;
    function GetRandomPiratessRank: Single;
    constructor Create; //override;
    destructor Destroy; override;
  end;

implementation
uses
  GameFlagship;

constructor TTier.Create(const ATier: Integer; const AMinRank, AMaxRank: Integer);
begin
  inherited Create; // empty
  Tier := ATier;
  MinRank := AMinRank;
  MaxRank := AMaxRank;
  Unlocked := false;
end;

function TTier.AverageRank: Single;
begin
  Result := (MinRank + MaxRank) / 2;
end;

function TTier.GetRandomRank: Single;
begin
  Result := MinRank + (MaxRank - MinRank) * Rnd.Random;
end;

{ TTiers -------------------------------------------------}

constructor TTiers.Create;
begin
  inherited; // parent is non-virtual and empty
  Tiers := TTiersDictionary.Create([doOwnsValues]);
end;

destructor TTiers.Destroy;
begin
  Tiers.Free;
  inherited;
end;

procedure TTiers.GenerateTiers;
var
  T: TTier;
begin
  T := TTier.Create(1, 20, 42);
  Tiers.Add(T.Tier, T);
  T := TTier.Create(2, 42, 64);
  Tiers.Add(T.Tier, T);
  T := TTier.Create(3, 64, 100);
  Tiers.Add(T.Tier, T);
  T := TTier.Create(4, 100, 121);
  Tiers.Add(T.Tier, T);
end;

function TTiers.MaxUnlockedTier: TTier;
var
  T: TTier;
  I: Integer;
begin
  I := 0;
  { Overall throughout the game we presume that
    Player cannot "jump over" tiers, but unlocks them sequentially }
  for T in Tiers.Values do
    if (T.Tier > I) and T.Unlocked then
      I := T.Tier;
  if I > 0 then
    Result := Tiers[I]
  else
    Result := nil;
end;

function TTiers.GetRandomPiratessRank: Single;
var
  I: Integer;
  PR: Single;
begin
  PR := Flagship.PlayerRank;
  if MaxUnlockedTier.MaxRank - 1 > PR then
    repeat
      I := 1 + Rnd.Random(4);
      Result := Tiers[I].GetRandomRank;
    until Tiers[I].Unlocked and (Result > PR)
  else
    Result := MaxUnlockedTier.MaxRank - Rnd.Random;
end;

end.

