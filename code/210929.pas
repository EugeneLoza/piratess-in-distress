
type
  TActressRenderLayer = class(TObject)
  strict private
  public
    Image: TCastleImage; // TODO to move it to cache [species, variant, pose, rl]
    ImageControl: TCastleImageControl;
    constructor Create; //override;
    destructor Destroy; override;
  end;
  TActressRenderLayersDictionary = specialize TObjectDictionary<String, TActressRenderLayer>;

type
  TActressPose = class(TObject)
  strict private
  public
    RenderLayers: TActressRenderLayersDictionary;
    constructor Create; //override;
    destructor Destroy; override;
  end;
  TActressPosesDictionary = specialize TObjectDictionary<String, TActressPose>;

type
  TActressLayerVariant = class(TObject)
  strict private
  public
    Poses: TActressPosesDictionary;
    constructor Create; //override;
    destructor Destroy; override;
  end;
  TActressLayerVariantsDictionary = specialize TObjectDictionary<String, TActressLayerVariant>;

type
  TActressLayer = class(TObject)
  strict private
  public
    Variants: TActressLayerVariantsDictionary;
    Variant: String;
    function CurrentVariant: TActressLayerVariant;
    constructor Create; //override;
    destructor Destroy; override;
  end;
  TActressLayersDictionary = specialize TObjectDictionary<String, TActressLayer>;

type
  TActressSpecies = class(TObject)
  strict private
  public
    Layers: TActressLayersDictionary;
    constructor Create; //override;
    destructor Destroy; override;
  end;
  TActressSpeciesDictionary = specialize TObjectDictionary<String, TActressSpecies>;
