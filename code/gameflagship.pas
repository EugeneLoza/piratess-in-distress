{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameFlagship;

{$include gamecompilerconfig.inc}

interface

uses
  Classes, Generics.Collections,
  GameGlobal, GameActress, GameTiers;

type
  TFloatSortedList = specialize TList<Single>;

const
  RankCoefficient = Single(1); // >= 0 (zero means the current Player's rank will be equal to highest rank captured piratess)

type
  TFlagship = class(TObject)
  strict private
    SortList: TFloatSortedList;
    HasEnchanting: Boolean;
    procedure GetBaseSortList;
    function CalculateRank: Single;
    function CalculateTamedRankBonus(const APiratess: TActress): Single;
  public
    Day: Integer;
    PlayerBaseRank: Single;
    ActressesDictionary: TActressesDictionary;
    Tiers: TTiers;
    { We don't bother saving LastTrack - it makes sense only in this run }
    LastTrack: String;
    function PlayerRank: Single;
    function PlayerPhysicalRank: Integer;
    function PlayerMaxPhysicalRank: Integer;
    function PropsCount: Integer;
    function PropsBonus: Integer;
    function PlayerRankBonus(const APiratess: TActress): Single;
    function GetMusic(const Playlist: TStringList): String;
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  Flagship: TFlagship;

implementation
uses
  SysUtils,
  CastleLog,
  GameGenerics, GameRanks;

constructor TFlagship.Create;
begin
  inherited; // empty
  SortList := TFloatSortedList.Create;
  ActressesDictionary := TActressesDictionary.Create([doOwnsValues]);
  Tiers := TTiers.Create;
  Tiers.GenerateTiers;
  LastTrack := '';
  // Will automatically increase to 1 the first battle.
  // Some actresses will spawn on day 0 that's ok, we only use those for comparison, never show
  Day := 0;
end;

destructor TFlagship.Destroy;
begin
  SortList.Free;
  ActressesDictionary.Free;
  Tiers.Free;
  inherited;
end;

function TFlagship.GetMusic(const Playlist: TStringList): String;
var
  Dictionary: TIntegerDictionary;
  A: TActress;
begin
  Dictionary := ConstructIntegerDictionary(Playlist);
  for A in ActressesDictionary.Values do
  begin
    if Dictionary.ContainsKey(A.CombatTheme) then
      Dictionary[A.CombatTheme] := Dictionary[A.CombatTheme] + 1
    else
    if Dictionary.ContainsKey(A.InterrogationTheme) then
      Dictionary[A.InterrogationTheme] := Dictionary[A.InterrogationTheme] + 1;
  end;
  Result := GetStringWithMinimalScore(Dictionary, LastTrack);
  LastTrack := Result;
  Dictionary.Free;
end;

function TFlagship.PlayerRank: Single;
begin
  GetBaseSortList;
  Result := CalculateRank;
end;

function TFlagship.PlayerPhysicalRank: Integer;
var
  R: TRank;
begin
  R := GetRank(PlayerRank);
  Result := R.X * R.Y;
end;

function TFlagship.PlayerMaxPhysicalRank: Integer;
begin
  Result := PlayerPhysicalRank + PropsBonus;
end;

function TFlagship.PropsCount: Integer;
begin
  Result := 1 + Trunc(Flagship.PlayerRank / 35);
end;

function TFlagship.PropsBonus: Integer;
begin
  Result := PropsCount * 5;
end;

function TFlagship.PlayerRankBonus(const APiratess: TActress): Single;
var
  CurrentRank: Single;
begin
  GetBaseSortList;
  if not APiratess.Tamed then
  begin
    CurrentRank := CalculateRank;
    SortList.Add(APiratess.VisibleRank);
    Result := CalculateRank - CurrentRank;
  end else
    Result := CalculateTamedRankBonus(APiratess)
end;

procedure TFlagship.GetBaseSortList;
var
  P: TActress;
begin
  HasEnchanting := false;
  SortList.Clear;
  for P in ActressesDictionary.Values do
    if P.Tamed then
    begin
      SortList.Add(P.TamedRank);
      if P.HasQuirk('Enchanting') then
      begin
        P.RevealQuirk('Enchanting');
        HasEnchanting := true;
      end;
    end;
end;

function TFlagship.CalculateRank: Single;
var
  I: Integer;
begin
  SortList.Sort(ComparerSingleAscending);
  Result := PlayerBaseRank;
  for I := 0 to Pred(SortList.Count) do
    if SortList[I] >= Result then
      Result := (RankCoefficient * Result + SortList[I]) / (RankCoefficient + 1.0)
    else
      WriteLnWarning('Inconsistency: Current Piratess rank is lower than average rank of her minors.'); // TODO: ignore if it's lower than PlayerBaseRank?
  if HasEnchanting then
    Result := MaxRank.X * MaxRank.Y;
end;

function TFlagship.CalculateTamedRankBonus(const APiratess: TActress): Single;
var
  I: Integer;
  R, R1: Single;
begin
  SortList.Sort(ComparerSingleAscending);
  R := PlayerBaseRank;
  for I := 0 to Pred(SortList.Count) do
  begin
    R1 := (RankCoefficient * R + SortList[I]) / (RankCoefficient + 1.0);
    if Abs(SortList[I] - APiratess.TamedRank) < 1e-4 then //BUG: This is unsafe, but who cares?
      Exit(R1 - R)
    else
      R := R1;
  end;
  raise Exception.Create('Cound not find the piratess!');
end;

finalization
  {Temporary}
  FreeAndNil(Flagship);

end.
