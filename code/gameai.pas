{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameAi;

{$include gamecompilerconfig.inc}

interface

uses
  Generics.Collections,
  GameSeaMap;

type
  TMove = record
    X, Y: Integer;
    Score: Single;
  end;
  TMovesList = specialize TList<TMove>;

{ Note: return of all functions is not managed, you have to free it manually }

{ Silly AI returns a set of available tiles for shooting }
function SillyAi(const Map: TSeaMap): TMovesList;
{ Basic AI:
  1. Finshes off known ships - highest priority
  2. If Player's ship is hit, tries to determine it's orientation - average priority
  3. Knows that Player doesn't have 1-sized ships, ignores such "holes"
  Doesn't range moves by how much hit or miss will be useful at this coordinates
  Doesn't account for player's ships remaining
  Doesn't account for player's moves history }
function BasicAi(const Map: TSeaMap): TMovesList;
{ Cheating AI: Returns all player's ships coordinates
  If there are mines - targets them exclusively }
function CheatingAi(const Map: TSeaMap): TMovesList;
{ Miss(ing) AI: Returns all empty tiles.
  Warning: return can have Count = 0 }
function MissAi(const Map: TSeaMap): TMovesList;

{ Not an AI, but helper for TSeaMap to get an exhaustive list of possible places to put mines }
function GetPossibleMinesPlaces(const Map: TSeaMap): TMovesList;

implementation
uses
  SysUtils,
  GameGenerics;

function SillyAi(const Map: TSeaMap): TMovesList;
var
  IX, IY: Integer;
  M: TMove;
begin
  Result := TMovesList.Create;
  for IX := 0 to Pred(Map.SizeX) do
    for IY := 0 to Pred(Map.SizeY) do
      if Map.CanShootHere(IX, IY) then
      begin
        M.X := IX;
        M.Y := IY;
        M.Score := 0.0;
        Result.Add(M);
      end;
end;

function BasicAi(const Map: TSeaMap): TMovesList;
var
  I, IX, IY: Integer;
  M: TMove;
  List: TMovesList;
  MaxScore: Single;

  procedure ProcessPossibleTarget(const DX, DY: Integer);
  begin
    M.X := IX + DX;
    M.Y := IY + DY;
    if Map.CanShootHere(M.X, M.Y) then
    begin
      if Map.ReadMapSafe(IX - DX, IY - DY) = mtShipHit then
        M.Score := 1.0
      else
        M.Score := 0.5;
      List.Add(M);
    end;
  end;

begin
  List := TMovesList.Create;
  for IX := 0 to Pred(Map.SizeX) do
    for IY := 0 to Pred(Map.SizeY) do
      if (Map.Map[IX, IY] = mtShipHit) then
      begin
        ProcessPossibleTarget( 1,  0); // they'll go into the list twice, but that doesn't matter, as entities with lower score will be discarded later
        ProcessPossibleTarget(-1,  0);
        ProcessPossibleTarget( 0,  1);
        ProcessPossibleTarget( 0, -1);
      end else
      if Map.CanShootHere(IX, IY) then
      begin
        if Map.CanShootHere(IX + 1, IY    ) or
           Map.CanShootHere(IX - 1, IY    ) or
           Map.CanShootHere(IX    , IY + 1) or
           Map.CanShootHere(IX    , IY - 1) then //there are no 1-sized ships for Player; "finishing off" Player's ships are handled by algorithm above
        begin
          M.X := IX;
          M.Y := IY;
          M.Score := 0.0;
          List.Add(M);
        end;
      end;
  if List.Count > 0 then
  begin
    List.Sort(ComparerMoveScore);
    MaxScore := List[0].Score;
    I := 0;
    repeat
      Inc(I);
    until (I = List.Count) or (List[I].Score < MaxScore * 0.95);
    //delete lower elements from the list
    List.Capacity := I; // I is at leat 1 here;
  end;
  Result := List;
end;

function CheatingAi(const Map: TSeaMap): TMovesList;
var
  IX, IY: Integer;
  M: TMove;
  Mines: TMovesList;
begin
  Mines := TMovesList.Create;
  Result := TMovesList.Create;
  for IX := 0 to Pred(Map.SizeX) do
    for IY := 0 to Pred(Map.SizeY) do
      if Map.Map[IX, IY] = mtShipAlive then
      begin
        M.X := IX;
        M.Y := IY;
        M.Score := 1.0;
        Result.Add(M);
      end else
      if Map.Map[IX, IY] = mtMine then
      begin
        M.X := IX;
        M.Y := IY;
        M.Score := 1.0;
        Mines.Add(M);
      end;
  if Mines.Count > 0 then
  begin
    Result.Free;
    Result := Mines;
  end else
    Mines.Free;
end;

function MissAi(const Map: TSeaMap): TMovesList;
var
  IX, IY: Integer;
  M: TMove;
begin
  Result := TMovesList.Create;
  for IX := 0 to Pred(Map.SizeX) do
    for IY := 0 to Pred(Map.SizeY) do
      if Map.Map[IX, IY] = mtUndefined then
      begin
        M.X := IX;
        M.Y := IY;
        M.Score := 1.0;
        Result.Add(M);
      end;
end;

function GetPossibleMinesPlaces(const Map: TSeaMap): TMovesList;
var
  IX, IY: Integer;
  M: TMove;
begin
  Result := TMovesList.Create;
  for IX := 0 to Pred(Map.SizeX) do
    for IY := 0 to Pred(Map.SizeY) do
      if Map.CanPutMineHere(IX, IY) then
      begin
        M.X := IX;
        M.Y := IY;
        M.Score := 1.0;
        Result.Add(M);
      end;
end;

end.

