{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GamePiratessButton;

{$include gamecompilerconfig.inc}

interface

uses
  Classes,
  Castlecontrols,
  GameActress, GameGlobal;

type
  TPiratessButton = class(TCastleButton)
  strict private
  public
    Actress: TActress;
    procedure Init(AActress: TActress);
  end;

implementation
uses
  SysUtils, CastleLog,
  GameFlagship;

procedure TPiratessButton.Init(AActress: TActress);
var
  S: String;
begin
  Actress := AActress;
  Self.Width := 1200;
  Self.Height := 200;
  Self.AutoSize := false;
  Self.EnableParentDragging := true;

  Self.Caption := Actress.Name;
  if Actress.Captured and not Actress.Tamed then
    Self.Caption := 'UNTAMED: ' + Self.Caption;
  if Actress.IsBoss then
    Self.Caption := Self.Caption + ' [BOSS]';

  if Actress.Captured then
    Self.Caption := Self.Caption + ' - ' + FloatToStringRounded(Actress.Rank, 0)
  else
    Self.Caption := Self.Caption + ' - ' + FloatToStringRounded(Actress.PhysicalRank.Value, 0);
  Self.Caption := Self.Caption + '(+' + FloatToStringRounded(Flagship.PlayerRankBonus(Actress), 1) + ')';
  for S in Actress.Quirks.Keys do
    if Actress.QuirkRevealed(S) then
      Self.Caption := Self.Caption + ' ' + Actress.QuirkToString(S);

  if not (Actress.Tamed or (Actress.HasQuirk('Revealing') and Actress.QuirkRevealed('Revealing')) or
    (Actress.Captured and not Actress.HasQuirk('Concealing'))) then
      Self.Caption := Self.Caption + ' ???';
end;

end.

