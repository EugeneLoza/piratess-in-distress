{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameGenerics;

{$include gamecompilerconfig.inc}

interface

uses
  Classes, Generics.Collections, Generics.Defaults,
  GameShip, GameActress, GameAi;

type
  TIntegerDictionary = specialize TDictionary<String, Integer>;

type
  TStringListHelper = class helper for TStringList
  public
    function Contains(const AString: String): Boolean;
  end;

type
  TComparerSingle = specialize TComparer<Single>;
  IComparerSingle = specialize IComparer<Single>;
  TComparerShip = specialize TComparer<TShip>;
  IComparerShip = specialize IComparer<TShip>;
  TComparerActress = specialize TComparer<TActress>;
  IComparerActress = specialize IComparer<TActress>;
  TComparerMove = specialize TComparer<TMove>;
  IComparerMove = specialize IComparer<TMove>;

var
  ComparerSingleAscending: IComparerSingle;
  ComparerShipTierDescending: IComparerShip;
  ComparerSevenSeas: IComparerActress;
  ComparerLowerDeck: IComparerActress;
  ComparerMoveScore: IComparerMove;

{ Construct a TIntegerDictionary and fills in with zeroes based on keys provided in AStringList
  Must manually free the result }
function ConstructIntegerDictionary(const AStringList: TStringList): TIntegerDictionary;
function GetStringWithMinimalScore(const ADictionary: TIntegerDictionary; const AvoidString: String): String;

procedure InitGenerics;
implementation
uses
  SysUtils,
  GameGlobal;

function TStringListHelper.Contains(const AString: String): Boolean;
var
  S: String;
begin
  for S in Self do
    if S = AString then
      Exit(true);
  Result := false;
end;

function ConstructIntegerDictionary(const AStringList: TStringList): TIntegerDictionary;
var
  S: String;
begin
  Result := TIntegerDictionary.Create;
  for S in AStringList do
    Result.Add(S, 0);
end;

function GetStringWithMinimalScore(const ADictionary: TIntegerDictionary; const AvoidString: String): String;
var
  S: String;
  List: TStringList;
  MinScore: Integer;
begin
  MinScore := MaxInt;
  for S in ADictionary.Keys do
    if (ADictionary[S] < MinScore) and (S <> AvoidString) then
      MinScore := ADictionary[S];
  List := TStringList.Create;
  for S in ADictionary.Keys do
    if (ADictionary[S] = MinScore) and (S <> AvoidString) then
      List.Add(S);
  Result := List[Rnd.Random(List.Count)];
  List.Free;
end;

function CompareSingleAscending(constref I1, I2: Single): Integer;
begin
  if I1 > I2 then
    Result := 1
  else
  if I2 > I1 then
    Result := -1
  else
    Result := 0;
end;

function CompareShipTierDescending(constref I1, I2: TShip): Integer;
begin
  if I1.Tier < I2.Tier then
    Result := 1
  else
  if I2.Tier < I1.Tier then
    Result := -1
  else
    Result := 0;
end;

function CompareActressSpawnDay(constref I1, I2: TActress): Integer;
begin
  if I1.SpawnedOnDay < I2.SpawnedOnDay then
    Result := 1
  else
  if I2.SpawnedOnDay < I1.SpawnedOnDay then
    Result := -1
  else
    Result := 0;
end;

function CompareActressCapturedTamedDay(constref I1, I2: TActress): Integer;
begin
  //sanity check
  if not I1.Captured or not I2.Captured then
    raise Exception.Create('Cannot compare - one of the piratesses is not captured!');
  // if both are tamed - compare taming day
  if I1.Tamed and I2.Tamed then
  begin
    if I1.TamedOnDay < I2.TamedOnDay then
      Result := 1
    else
    if I2.TamedOnDay < I1.TamedOnDay then
      Result := -1
    else
      Result := 0;
  end else
  // if both are not tamed - compare capture day
  if not I1.Tamed and not I2.Tamed then
  begin
    if I1.CapturedOnDay < I2.CapturedOnDay then
      Result := 1
    else
    if I2.CapturedOnDay < I1.CapturedOnDay then
      Result := -1
    else
      Result := 0;
  end else
  // otherwise always show untamed before tamed
  if I1.Tamed and not I2.Tamed then
    Result := 1
  else
  if not I1.Tamed and I2.Tamed then
    Result := -1
  else
    raise Exception.Create('Something''s wrong with conditions in CompareActressCapturedTamedDay');
end;

function CompareMoveScoreDescending(constref I1, I2: TMove): Integer;
begin
  if I1.Score < I2.Score then
    Result := 1
  else
  if I2.Score < I1.Score then
    Result := -1
  else
    Result := 0;
end;

procedure InitGenerics;
begin
  ComparerSingleAscending := TComparerSingle.Construct(@CompareSingleAscending);
  ComparerShipTierDescending := TComparerShip.Construct(@CompareShipTierDescending);
  ComparerSevenSeas := TComparerActress.Construct(@CompareActressSpawnDay);
  ComparerLowerDeck := TComparerActress.Construct(@CompareActressCapturedTamedDay);
  ComparerMoveScore := TComparerMove.Construct(@CompareMoveScoreDescending);
end;

end.

