{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameActress;

{$include gamecompilerconfig.inc}

interface

uses
  Generics.Collections,
  Classes,
  CastleImages, CastleControls, CastleUiControls,
  {Temporary}GameShip, GameShipsManager,
  GameRanks, GameActressPose, GameScreenEffect, GameQuirks, GameSounds, GameGlobal;

type
  TPose = (pNormal);

type
  TActressQuirks = specialize TObjectDictionary<String, TQuirkData>;

type
  TRopeRecord = Single;
  TRopesList = specialize TList<TRopeRecord>;

type
  TActress = class(TObject)
  strict private
    FCaptured: Boolean;
    FTamed: Boolean;
    Ropes: TRopesList;
    procedure SetCaptured(const Value: Boolean);
    procedure SetTamed(const Value: Boolean);
    procedure CreateShips;
    procedure GetRandomQuirks;
    procedure AttackSelf(const TargetMap: TObject);
    procedure Attack(const TargetMap: TObject);
    procedure TryEscape;
    procedure InitMusic;
  public
    Id: String;
    Name: String;
    Rank: Single;
    Quirks: TActressQuirks;
    Body: TCastleUserInterface;
    Poses: array[TPose] of TActressPose;
    OnRevealQuirk: TSimpleProcedure;
    {temporary} Ships: TShipsManager;
    CombatTheme: String;
    InterrogationTheme: String;
    { Used to sort in Seven Seas }
    SpawnedOnDay: Integer;
    { Used to sort in LowerDeck }
    CapturedOnDay: Integer;
    TamedOnDay: Integer;
    UnlocksTier: Integer;
    function IsBoss: Boolean;
    function IsBound: Boolean;
    procedure SetKnotsActive(const Value: Integer);
    procedure ApplyRope(const RopeHp: Single; const Adhesive: Boolean);
    procedure DoSillyAi(const TargetMap: TObject);
    procedure DoAi(const TargetMap, OwnMap: TObject);
    property Captured: Boolean read FCaptured write SetCaptured;
    property Tamed: Boolean read FTamed write SetTamed;
    function PhysicalRank: TRank;
    function VisibleRank: Single;
    {
      Also reveals related quirks when called }
    function TamedRank: Single;
    function HasQuirk(const AQuirk: String): Boolean;
    procedure RevealQuirk(const AQuirk: String; const ForceReveal: Boolean = false);
    procedure AddQuirk(const AQuirk: String; const AValue: TQuirkValue);
    function QuirkRevealed(const AQuirk: String): Boolean;
    function QuirkValueMultiplier(const AQuirk: String): Single;
    function QuirkValueInteger(const AQuirk: String): Integer;
    function QuirkValueChance(const AQuirk: String): Boolean;
    function QuirkToString(const AQuirk: String): String;
    procedure RevealAllQuirks(const ForceReveal: Boolean = false);
    procedure Init(const AName: String; const ARank: Single; const AUnlocksTier: Integer = -1);
    procedure PrepareForBattle;
    constructor Create; //override;
    destructor Destroy; override;
  end;
  TActressesList = specialize TObjectList<TActress>;
  TActressesDictionary = specialize TObjectDictionary<String, TActress>;

implementation
uses
  SysUtils,
  CastleLog,
  GameNamesGenerator, GameGenerics, GameFlagship,
  GameSeaMap, GameSeaMapInterface, GameAi;

function TActress.IsBoss: Boolean;
begin
  Result := UnlocksTier > 0;
end;

function TActress.IsBound: Boolean;
begin
  Result := Ropes.Count > 0;
end;

procedure TActress.SetKnotsActive(const Value: Integer);
var
  Layer: TActressLayer;
begin
  for Layer in Poses[pNormal].Layers do
    if Layer.Knots then
      Layer.IsActive := Value > 0; // TODO: multiple layers for different ropes
end;

procedure TActress.TryEscape;
const
  RopeEscapeSkill = 50;
var
  ThisRope: Integer;
  EscapeAbility: Single;
  I: Integer;
begin
  ThisRope := Rnd.Random(Ropes.Count);
  EscapeAbility := RopeEscapeSkill * Rnd.Random;
  if HasQuirk('Escapist') then
  begin
    RevealQuirk('Escapist');
    EscapeAbility *= QuirkValueMultiplier('Escapist');
  end;
  if HasQuirk('Entangled') then
  begin
    RevealQuirk('Entangled');
    EscapeAbility *= QuirkValueMultiplier('Entangled');
  end;
  for I := 2 to Ropes.Count do
    EscapeAbility *= 0.8;
  Ropes[ThisRope] := Ropes[ThisRope] - EscapeAbility;

  for I := 0 to Pred(Ropes.Count) do
    WriteLnLog(I.ToString, Ropes[I].ToString);

  if Ropes[ThisRope] < 0 then
    Ropes.Delete(ThisRope);
  SetKnotsActive(Ropes.Count);
end;

procedure TActress.DoSillyAi(const TargetMap: TObject);
var
  MoveList: TMovesList;
  M: TMove;
begin
  MoveList := SillyAi((TargetMap as TSeaMapInterface).Map);
  if MoveList.Count > 0 then
  begin
    M := MoveList[Rnd.Random(MoveList.Count)];
    (TargetMap as TSeaMapInterface).ShotHere(M.X, M.Y);
  end else
    WriteLnLog('Move list is empty. Cannot attack.');
  MoveList.Free;
end;

procedure TActress.DoAi(const TargetMap, OwnMap: TObject);
begin
  if IsBound then
  begin
    TryEscape;
    Exit; // even if escaped - skips turn
  end;
  if HasQuirk('Confused') and QuirkValueChance('Confused') then
  begin
    RevealQuirk('Confused');
    AttackSelf(OwnMap);
  end else
  begin
    Attack(TargetMap);
    if HasQuirk('Doppelgangster') and QuirkValueChance('Doppelgangster') then
    begin
      RevealQuirk('Doppelgangster');
      Attack(TargetMap);
    end;
    if HasQuirk('Ambidext') and not Ships.NoLargeTierShipsAlive then
    begin
      RevealQuirk('Ambidext');
      Attack(TargetMap);
    end;
    if HasQuirk('Fierce') and Ships.HasFlagship then
    begin
      RevealQuirk('Fierce');
      Attack(TargetMap);
    end;
  end;
end;

procedure TActress.AttackSelf(const TargetMap: TObject);
var
  MoveList: TMovesList;
  M: TMove;
  TargetSeaMap: TSeaMapInterface;
begin
  TargetSeaMap := TargetMap as TSeaMapInterface;
  MoveList := SillyAi(TargetSeaMap.Map); // uses only Silly AI for that, not to make Confused perk too nerfing
  if MoveList.Count > 0 then
  begin
    M := MoveList[Rnd.Random(MoveList.Count)];
    TargetSeaMap.ShotHere(M.X, M.Y);
  end else
    WriteLnLog('Move list is empty. Cannot attack.');
  MoveList.Free;
end;

procedure TActress.Attack(const TargetMap: TObject);
var
  MoveList: TMovesList;
  M: TMove;
  TargetSeaMap: TSeaMapInterface;
begin
  TargetSeaMap := TargetMap as TSeaMapInterface;
  if HasQuirk('Skipper') and QuirkValueChance('Skipper') then
  begin
    RevealQuirk('Skipper');
    Exit;
  end;

  if HasQuirk('Cheating') and QuirkValueChance('Cheating') then
  begin
    RevealQuirk('Cheating');
    MoveList := CheatingAi(TargetSeaMap.Map);
  end else
  if HasQuirk('Miss') and QuirkValueChance('Miss') then
  begin
    MoveList := MissAi(TargetSeaMap.Map);
    if (MoveList.Count = 0) then
    begin
      MoveList.Free;
      MoveList := SillyAi(TargetSeaMap.Map);
    end else
      RevealQuirk('Miss');
  end else
  if HasQuirk('Silly') and QuirkValueChance('Silly') then
  begin
    RevealQuirk('Silly');
    MoveList := SillyAi(TargetSeaMap.Map);
  end else
  {----- BOSS PERKS -----}
  if HasQuirk('Tutorial') then
  begin
    RevealQuirk('Tutorial');
    MoveList := MissAi(TargetSeaMap.Map);
    if (MoveList.Count = 0) then
    begin
      MoveList.Free;
      MoveList := SillyAi(TargetSeaMap.Map);
    end;
  end else
  if HasQuirk('Clairvoyant') and not Ships.NoLargeTierShipsAlive then
  begin
    RevealQuirk('Clairvoyant');
    MoveList := CheatingAi(TargetSeaMap.Map);
  end else
  {---- DEFAULT : BASIC AI ----}
    MoveList := BasicAi(TargetSeaMap.Map); //default

  if MoveList.Count > 0 then
  begin
    M := MoveList[Rnd.Random(MoveList.Count)];
    TargetSeaMap.ShotHere(M.X, M.Y);
  end else
    WriteLnLog('Move list is empty. Cannot attack.');
  MoveList.Free;
end;

procedure TActress.ApplyRope(const RopeHp: Single; const Adhesive: Boolean);
begin
  if not Adhesive and not IsBound and HasQuirk('Untamed') and QuirkValueChance('Untamed') then
  begin
    RevealQuirk('Untamed');
    // TODO: Animate rope miss
    Exit;
  end;
  if not Adhesive and not IsBound and HasQuirk('Random') then
  begin
    RevealQuirk('Random');
    // TODO: Animate rope miss
    Exit;
  end;
  if not Adhesive and not IsBound and HasQuirk('Unyielding') then
  begin
    RevealQuirk('Unyielding');
    // TODO: Animate rope miss
    Exit;
  end;
  Ropes.Add(RopeHp);
  SetKnotsActive(Ropes.Count);
end;

function TActress.PhysicalRank: TRank;
begin
  Result := GetRank(Rank);
end;

function TActress.VisibleRank: Single;
begin
  if HasQuirk('Helpful') and QuirkRevealed('Helpful') then
    Result := Rank * QuirkValueMultiplier('Helpful')
  else
  if HasQuirk('Useless') and QuirkRevealed('Useless') then
    Result := MinRank.Value + (Rank - MinRank.Value) * QuirkValueMultiplier('Useless')
  else
    Result := Rank;
end;

function TActress.TamedRank: Single;
begin
  if HasQuirk('Helpful') then
  begin
    RevealQuirk('Helpful');
    Result := Rank * QuirkValueMultiplier('Helpful');
  end else
  if HasQuirk('Useless') then
  begin
    RevealQuirk('Useless');
    Result := MinRank.Value + (Rank - MinRank.Value) * QuirkValueMultiplier('Useless');
  end else
    Result := Rank;
end;

function TActress.HasQuirk(const AQuirk: String): Boolean;
begin
  if not QuirksDictionary.ContainsKey(AQuirk) then
    raise Exception.Create('Quirk is not found in the dictionary: ' + AQuirk);
  Result := Quirks.ContainsKey(AQuirk);
end;

procedure TActress.AddQuirk(const AQuirk: String; const AValue: TQuirkValue);
begin
  if not QuirksDictionary.ContainsKey(AQuirk) then
    raise Exception.Create('Quirk is not found in the dictionary: ' + AQuirk);
  Quirks.Add(AQuirk, QuirkData(AValue));
end;

procedure TActress.RevealQuirk(const AQuirk: String; const ForceReveal: Boolean = false);
begin
  if not HasQuirk(AQuirk) then // will also check dictionary
    raise Exception.Create('Cannot reveal quirk, as the piratess doesn''t have this quirk: ' + AQuirk);
  if Quirks[AQuirk].Status = qrHidden then
  begin
    if ForceReveal or (AQuirk = 'Concealing') or not HasQuirk('Concealing') then
    begin
      Quirks[AQuirk].Status := qrRevealPending;
      WriteLnLog('Quirk has been revealed: %S (%D)', [AQuirk, Quirks[AQuirk].Value]);
    end else
      RevealQuirk('Concealing');
    if (AQuirk = 'Revealing') then
      RevealAllQuirks;
    if Assigned(OnRevealQuirk) then
      OnRevealQuirk;
  end;
end;

function TActress.QuirkRevealed(const AQuirk: String): Boolean;
begin
  if not HasQuirk(AQuirk) then // will also check dictionary
    raise Exception.Create('Cannot check revealed quirk, as the piratess doesn''t have this quirk: ' + AQuirk);
  Result := Quirks[AQuirk].Status <> qrHidden;
end;

{$warn 6018 off}
function TActress.QuirkValueMultiplier(const AQuirk: String): Single;
begin
  if not HasQuirk(AQuirk) then // will also check dictionary
    raise Exception.Create('Cannot get quirk value, as the piratess doesn''t have this quirk: ' + AQuirk);
  case QuirkInfo(AQuirk).ValueType of
    qvBoolean: raise Exception.Create('Quirk "' + AQuirk + '" is Boolean, it doesn'' have a value');
    qvInteger: raise Exception.Create('Quirk "' + AQuirk + '" is Integer, cannot ask for multiplier.');
    qvPositiveMultiplier: Result := 1.0 + Quirks[AQuirk].Value / 100.0;
    qvNegativeMultiplier: Result := 1.0 - Quirks[AQuirk].Value / 100.0;
    qvChance: raise Exception.Create('Quirk "' + AQuirk + '" is Chance, cannot ask for multiplier.');
    else
      raise Exception.Create('Unexpected QuirkValueType');
  end;
end;

function TActress.QuirkValueInteger(const AQuirk: String): Integer;
begin
  if not HasQuirk(AQuirk) then // will also check dictionary
    raise Exception.Create('Cannot get quirk value, as the piratess doesn''t have this quirk: ' + AQuirk);
  case QuirkInfo(AQuirk).ValueType of
    qvBoolean: raise Exception.Create('Quirk "' + AQuirk + '" is Boolean, it doesn'' have a value');
    qvInteger: Result := Quirks[AQuirk].Value;
    qvPositiveMultiplier: raise Exception.Create('Quirk "' + AQuirk + '" is Multiplier, cannot ask for Integer value.');
    qvNegativeMultiplier: raise Exception.Create('Quirk "' + AQuirk + '" is Multiplier, cannot ask for Integer value.');
    qvChance: raise Exception.Create('Quirk "' + AQuirk + '" is Chance, cannot ask for Integer value.');
    else
      raise Exception.Create('Unexpected QuirkValueType');
  end;
end;

function TActress.QuirkValueChance(const AQuirk: String): Boolean;
begin
  if not HasQuirk(AQuirk) then // will also check dictionary
    raise Exception.Create('Cannot get quirk value, as the piratess doesn''t have this quirk: ' + AQuirk);
  case QuirkInfo(AQuirk).ValueType of
    qvBoolean: raise Exception.Create('Quirk "' + AQuirk + '" is Boolean, it doesn'' have a chance.');
    qvInteger: raise Exception.Create('Quirk "' + AQuirk + '" is Chance, cannot ask for Chance value.');
    qvPositiveMultiplier: raise Exception.Create('Quirk "' + AQuirk + '" is Multiplier, cannot ask for Chance value.');
    qvNegativeMultiplier: raise Exception.Create('Quirk "' + AQuirk + '" is Multiplier, cannot ask for Chance value.');
    qvChance: Result := Rnd.Random < Quirks[AQuirk].Value / 100;
    else
      raise Exception.Create('Unexpected QuirkValueType');
  end;
end;

function TActress.QuirkToString(const AQuirk: String): String;
begin
  Result := AQuirk;
  case QuirkInfo(AQuirk).ValueType of
    qvBoolean: {nop};
    qvInteger: Result += '(' + Quirks[AQuirk].Value.ToString + ')';
    qvPositiveMultiplier, qvNegativeMultiplier, qvChance: Result += '(' + Quirks[AQuirk].Value.ToString + '%)';
    else
      raise Exception.Create('Unexpected QuirkValueType');
  end;
end;
{$warn 6018 on}

procedure TActress.RevealAllQuirks(const ForceReveal: Boolean = false);
var
  Q: String;
begin
  for Q in Quirks.Keys do
    RevealQuirk(Q, ForceReveal);
end;

procedure TActress.SetTamed(const Value: Boolean);
begin
  if FTamed <> Value then
  begin
    if Value then
    begin
      RevealAllQuirks(true); // maybe additional dialogue option?
      TamedOnDay := Flagship.Day;
    end;
    FTamed := Value;
  end;
end;

procedure TActress.SetCaptured(const Value: Boolean);
begin
  if FCaptured <> Value then
  begin
    if Value then
    begin
      RevealAllQuirks(false);
      CapturedOnDay := Flagship.Day;
    end;
    FCaptured := Value;
  end;
end;

procedure TActress.Init(const AName: String; const ARank: Single;
  const AUnlocksTier: Integer);
begin
  if AName = '' then
    Name := GenerateRandomName
  else
    Name := AName;

  SpawnedOnDay := Flagship.Day;
  Rank := ARank;
  UnlocksTier := AUnlocksTier;

  Poses[pNormal] := TActressPose.Create;
  Body := TCastleUserInterface.Create(nil);
  Body.FullSize := true;
  Body.InsertFront(Poses[pNormal].Pose);

  Quirks := TActressQuirks.Create([doOwnsValues]);

  GetRandomQuirks;

  CombatTheme := '';

  CreateShips;
end;

procedure TActress.InitMusic;
begin
  if CombatTheme = '' then
  begin
    if PhysicalRank.Value >= Flagship.PlayerMaxPhysicalRank then
      CombatTheme := Flagship.GetMusic(MusicsHardBattle)
    else
      CombatTheme := Flagship.GetMusic(MusicsEasyBattle);
    InterrogationTheme := CombatTheme;
  end;
end;

procedure TActress.PrepareForBattle;
begin
  InitMusic;
  Ropes.Clear;
end;

procedure TActress.GetRandomQuirks;
begin
  if Rnd.Random < 0.2 then
    if Rnd.RandomBoolean then
      AddQuirk('Helpful', 5 + Round(Sqr(Rnd.Random) * 45))
    else
      AddQuirk('Useless', 5 + Round(Sqr(Rnd.Random) * 45));
  if Rnd.Random < 0.1 then
    if Rnd.RandomBoolean then
      AddQuirk('Shrimped', 1)
    else
      if Rank >= 20 then // ship sized 4 (min large ship, tier 3) turns into 5 and can't fit into 4x4 field
        if Rank >= 30 then
          if Rank >= 42 then
            AddQuirk('Elongated', 1 + Round(Sqr(Sqr(Sqr(Sqr(Rnd.Random)))) * 2)) // +3 max
          else
            AddQuirk('Elongated', 1 + Round(Sqr(Sqr(Rnd.Random)))) // +2 max
        else
          AddQuirk('Elongated', 1);
  if Rnd.Random < 0.1 then
    AddQuirk('Submissive', 1);
  if Rnd.Random < 0.1 then
    AddQuirk('Cheating', 3 + Round(Sqr(Rnd.Random) * 30));
  if Rnd.Random < 0.1 then
    AddQuirk('Doppelgangster', 10 + Round(Sqr(Rnd.Random) * 15));
  if Rnd.Random < 0.1 then
    AddQuirk('Miss', 10 + Round(Sqr(Rnd.Random) * 40));
  if Rnd.Random < 0.1 then
    AddQuirk('Skipper', 3 + Round(Sqr(Rnd.Random) * 22));
  if Rnd.Random < 0.1 then
    AddQuirk('Silly', 25 + Round(Sqrt(Rnd.Random) * 75));
  if Rnd.Random < 0.1 then
    AddQuirk('Confused', 3 + Round(Sqrt(Rnd.Random) * 17));
  if Rnd.Random < 0.1 then
    if Rnd.RandomBoolean then
      AddQuirk('Shy', 1)
    else
      AddQuirk('Shameless', 1);
  if Rnd.Random < 0.1 then
    if Rnd.RandomBoolean then
      AddQuirk('Escapist', 20 + Round(Rnd.Random * 30))
    else
      AddQuirk('Entangled', 20 + Round(Sqr(Rnd.Random) * 60));
  if Rnd.Random < 0.1 then
    if Rnd.RandomBoolean then
      AddQuirk('Rushing', 1 + Round(Rnd.Random * Rank / 10))
    else
      AddQuirk('Lagging', 1 + Round(Rnd.Random * Rank / 10));
  if Rnd.Random < 0.1 then
    AddQuirk('Untamed', 25 + Round(Rnd.Random * 75));
  if Rnd.Random < 0.1 then
    AddQuirk('Miner', 1 + Round(Rnd.Random * Rank / 30));
  if (Quirks.Count > 0) and (Rnd.Random < 0.4) then
    if Rnd.RandomBoolean then
      AddQuirk('Concealing', 1)
    else
      AddQuirk('Revealing', 1);
end;

procedure TActress.CreateShips;
const
  ShipsDensity = 0.3; // tried 0.3 - a bit more random start; 0.35 - no difference felt; 0.4 - too obvious ending; 0.45 - often fails at rank 20; 0.5 sometimes fails even at rank 36
var
  Ship: TShip;
  Layer: TActressLayer;
  WorkLayers: TActressLayerList;
  ExpectedShips: Integer;
  MaxShipsTier: Integer;
  TotalShips: Integer;
  CoverTop, CoverBottom: Boolean;
  SmallShips, LargeShips: Integer;
  RankRec: TRank;
  LargerSide: Integer;
  HasNoLargeShips: Boolean;

  procedure AddShipForLayer(const ShipLayer: TActressLayer);
  begin
    Ship := TShip.Create;
    Ship.Owner := Self;
    Ship.Layer := Layer;
    Ship.Init(Layer.Tier);
    Ships.Ships.Add(Ship);
    TotalShips += Ship.Length;
    CoverTop := CoverTop or Layer.CoversTop;
    CoverBottom := CoverBottom or Layer.CoversBottom;
    if Layer.Tier < MinimalLargeShipTier then
      Inc(SmallShips)
    else
      Inc(LargeShips);
    WorkLayers.Remove(Layer);
  end;

begin
  WriteLnLog('Generating ships for ' + Name + ' Rank: ' + Rank.ToString);
  if HasQuirk('Elongated') then
    WriteLnLog('isElongated');
  RankRec := GetRank(Rank);
  if RankRec.X < RankRec.Y then
    LargerSide := RankRec.Y
  else
    LargerSide := RankRec.X;
  if HasQuirk('Shrimped') then
    LargerSide += QuirkValueInteger('Shrimped');
  if HasQuirk('Elongated') then
    LargerSide -= QuirkValueInteger('Elongated');
  ExpectedShips := Round(Rank * ShipsDensity);
  MaxShipsTier := 0;
  for Layer in Poses[pNormal].Layers do
    MaxShipsTier += Layer.Tier;

  Ships := TShipsManager.Create;
  // inefficient and temporary
  WorkLayers := TActressLayerList.Create(false);
  repeat
    Ships.Ships.Clear;
    WorkLayers.Clear;
    HasNoLargeShips := true;
    for Layer in Poses[pNormal].Layers do
      if (Layer.Tier > 0) and (Layer.Tier + 1 <= LargerSide) then
      begin
        WorkLayers.Add(Layer);
        if Layer.Tier >= MinimalLargeShipTier then
          HasNoLargeShips := false;
      end;
    CoverTop := false;
    CoverBottom := false;
    SmallShips := 0;
    LargeShips := 0;
    TotalShips := 0;

    if HasQuirk('Shy') then // Shy piratess should have full set of underwear
      for Layer in Poses[pNormal].Layers do
        if Layer.IsUnderwear and (Layer.CoversBottom or (Rank >= 36) or ((Rank >= 30) and not HasQuirk('Elongated'))) then
          AddShipForLayer(Layer);
    repeat
      Layer := WorkLayers[Rnd.Random(WorkLayers.Count)];
      AddShipForLayer(Layer);
    until (TotalShips >= ExpectedShips) or (WorkLayers.Count = 0);
  until CoverTop and CoverBottom and (SmallShips > 0) and ((LargeShips > 0) or HasNoLargeShips) and (TotalShips <= ExpectedShips + 1)
    and ((LargeShips = 1) or (Name <> 'Insecte')); // workaround the issue that we don't control clothes levels yet
  WorkLayers.Free;
  //...
  for Layer in Poses[pNormal].Layers do
    if Layer.Tier > 0 then
      Layer.IsActive := false;
  // TODO: move to TShips.Init
  for Ship in Ships.Ships do
    Ship.Layer.IsActive := true;

  Ships.Ships.Sort(ComparerShipTierDescending);

  for Ship in Ships.Ships do
    WriteLnLog('ship', Ship.Length.ToString);
end;

constructor TActress.Create;
begin
  inherited;
  id := 'i' + Rnd.Random32bit.ToString;
  FTamed := false;
  FCaptured := false;
  CapturedOnDay := -1;
  TamedOnDay := -1;
  Ropes := TRopesList.Create;
end;

destructor TActress.Destroy;
var
  P: TPose;
begin
  for P in TPose do
    Poses[P].Free;
  Body.Free;
  Quirks.Free;
  Ropes.Free;
  {temporary} Ships.Free;
  inherited;
end;

end.

