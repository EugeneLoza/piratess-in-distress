{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameSounds;

{$include gamecompilerconfig.inc}

interface
uses
  Classes;

var
  MusicsEasyBattle: TStringList;
  MusicsHardBattle: TStringList;
  MusicsShip: TStringList;

procedure SoundRipLong;
procedure SoundRipMedium;
procedure SoundRipShort;
procedure SoundShot;
procedure SoundMiss;
procedure SoundHit;

procedure SoundClickForward;
procedure SoundClickBack;

procedure SoundLastPlayerShipWarning;

procedure InitializeSounds;

implementation
uses
  SysUtils,
  CastleDownload, DOM, CastleXmlUtils, XmlRead,
  CastleSoundEngine, CastleConfig, CastleLog, {$ifdef ANDROID}CastleOpenDocument,{$endif}
  GameGlobal;

var
  SoundsClickForward: TStringList;
  SoundsClickBack: TStringList;
  SoundsRipLong: TStringList;
  SoundsRipMedium: TStringList;
  SoundsRipShort: TStringList;
  SoundsPlayerShot: TStringList;
  SoundsPlayerMiss: TStringList;
  SoundsPlayerHit: TStringList;
  // Special music/sounds are treated separatedly - these are only those up for randomization

procedure InitializeSounds;
var
  Stream: TStream;
  SoundDoc: TXMLDocument;
  SName, SNameLower: String;
  I: TXMLElementIterator;
begin
  SoundEngine.RepositoryURL := 'castle-data:/audio/index.xml';

  MusicsEasyBattle := TStringList.Create;
  MusicsHardBattle := TStringList.Create;
  MusicsShip := TStringList.Create;
  MusicsShip.Add('menu_music'); // this is index 0
  SoundsRipLong := TStringList.Create;
  SoundsRipMedium := TStringList.Create;
  SoundsRipShort := TStringList.Create;
  SoundsPlayerShot := TStringList.Create;
  SoundsPlayerMiss := TStringList.Create;
  SoundsPlayerHit := TStringList.Create;
  SoundsClickForward := TStringList.Create;
  SoundsClickBack := TStringList.Create;

  { WORKAROUND Alias no longer randomizing sounds and SoundEngine.FSound private }
  Stream := Download(SoundEngine.RepositoryURL);
  try
    ReadXMLFile(SoundDoc, Stream);
  finally
    FreeAndNil(Stream);
  end;
  I := SoundDoc.DocumentElement.ChildrenIterator;
  try
    while I.GetNext do
      if I.Current.TagName = 'sound' then
      begin
        SName := I.Current.AttributeString('name');
        SNameLower := LowerCase(SName);

        //music
        if Pos('hard_battle', SNameLower) > 0 then
          MusicsHardBattle.Add(SName)
        else if Pos('extreme_battle', SNameLower) > 0 then
        begin
          MusicsHardBattle.Add(SName);
          //TODO: MusicsExtremeBattle.Add(SName);
        end
        else if Pos('easy_battle', SNameLower) > 0 then
          MusicsEasyBattle.Add(SName)
        else if Pos('ship_music', SNameLower) > 0 then
          MusicsShip.Add(SName)

        //ui
        else if Pos('click_forward', SNameLower) > 0 then
          SoundsClickForward.Add(SName)
        else if Pos('click_back', SNameLower) > 0 then
          SoundsClickBack.Add(SName)

        //battle
        else if Pos('rip_long', SNameLower) > 0 then
          SoundsRipLong.Add(SName)
        else if Pos('rip_medium', SNameLower) > 0 then
          SoundsRipMedium.Add(SName)
        else if Pos('rip_short', SNameLower) > 0 then
          SoundsRipShort.Add(SName)
        else if Pos('player_shot', SNameLower) > 0 then
          SoundsPlayerShot.Add(SName)
        else if Pos('player_miss', SNameLower) > 0 then
          SoundsPlayerMiss.Add(SName)
        else if Pos('player_hit', SNameLower) > 0 then
          SoundsPlayerHit.Add(SName);
      end
      else
        WriteLnWarning('Unexpected Sound tag :' + I.Current.TagName);
  finally
    FreeAndNil(I);
  end;
  FreeAndNil(SoundDoc);
  { /WORKAROUND }

  SoundEngine.Volume := UserConfig.GetFloat('volume', 1.0);
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music', 1.0);
end;

procedure FinalizeSounds;
begin
  MusicsEasyBattle.Free;
  MusicsHardBattle.Free;
  MusicsShip.Free;
  SoundsClickForward.Free;
  SoundsClickBack.Free;
  SoundsRipLong.Free;
  SoundsRipMedium.Free;
  SoundsRipShort.Free;
  SoundsPlayerShot.Free;
  SoundsPlayerMiss.Free;
  SoundsPlayerHit.Free;
end;

procedure SoundRipLong;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsRipLong[Rnd.Random(SoundsRipLong.Count)]));
end;

procedure SoundRipMedium;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsRipMedium[Rnd.Random(SoundsRipMedium.Count)]));
end;

procedure SoundRipShort;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsRipShort[Rnd.Random(SoundsRipShort.Count)]));
end;

procedure SoundShot;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsPlayerShot[Rnd.Random(SoundsPlayerShot.Count)]));
end;

procedure SoundMiss;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsPlayerMiss[Rnd.Random(SoundsPlayerMiss.Count)]));
end;

procedure SoundHit;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsPlayerHit[Rnd.Random(SoundsPlayerHit.Count)]));
end;

procedure SoundClickForward;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsClickForward[Rnd.Random(SoundsClickForward.Count)]));
end;
procedure SoundClickBack;
begin
  SoundEngine.Play(SoundEngine.SoundFromName(SoundsClickBack[Rnd.Random(SoundsClickBack.Count)]));
end;

procedure SoundLastPlayerShipWarning;
begin
  //SoundEngine.Play(SoundEngine.SoundFromName('last-ship-warning'); // TODO
  {$ifdef ANDROID}
  if UserConfig.GetValue('vibration', true) then
    Vibrate(500);
  {$endif}
end;

finalization
  FinalizeSounds;
end.

