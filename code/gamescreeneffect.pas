{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameScreenEffect;

{$include gamecompilerconfig.inc}

// Some changes wait to be merged into MASTER
{$define CGE_CHANGE_CLASS_UNMERGED}
//{$define CGE_BLENDING_UNMERGED}

interface

uses
  Classes,
  CastleScreenEffects, X3DNodes, X3DLoad, CastleTimeUtils,
  CastleImages, CastleColors, CastleUiControls;

type
  TBurnScreenEffect = class(TCastleScreenEffects)
  strict private
    BurnRoot: TX3DRootNode;
    BurnEffect: TScreenEffectNode;
  public
    constructor Create(AOwenr: TComponent); override;
    destructor Destroy; override;
  end;

{ Detaches all children from EffectParent and packs them back
  into TCastleScreenEffects, adding a transparent TCastleSimpleBackground underneath }
procedure ApplyEffect(const EffectParent: TCastleUserInterface);

implementation
uses
  CastleControls, CastleVectors;

procedure ApplyEffect(const EffectParent: TCastleUserInterface);
var
  BurnEffect: TBurnScreenEffect;
  Background: TCastleSimpleBackground;
  {$ifdef CGE_CHANGE_CLASS_UNMERGED}
  ThisControl: TCastleUserInterface;
  {$endif}
  I: Integer;
begin
  BurnEffect := TBurnScreenEffect.Create(EffectParent);
  BurnEffect.FullSize := true;
  for I := EffectParent.ControlsCount - 1 downto 0 do
  begin
    {$ifndef CGE_CHANGE_CLASS_UNMERGED}
    BurnEffect.InsertFront(EffectParent.ExtractControl(I));
    {$else}
    ThisControl := EffectParent.Controls[I];
    EffectParent.RemoveControl(ThisControl);
    BurnEffect.InsertFront(ThisControl);
    {$endif}
  end;
  Background := TCastleSimpleBackground.Create(BurnEffect);
  Background.Color := Vector4(1, 0, 0, 0);
  Background.FullSize := true;
  BurnEffect.InsertBack(Background);
  EffectParent.InsertFront(BurnEffect);
end;

constructor TBurnScreenEffect.Create(AOwenr: TComponent);
begin
  inherited;
  FullSize := true;

  BurnRoot := LoadNode('castle-data:/shaders/burn.x3dv');
  BurnEffect := BurnRoot.FindNode('BurnScreenEffect') as TScreenEffectNode;
  {$ifndef CGE_BLENDING_UNMERGED}
  AddScreenEffect(BurnEffect);
  Blending := true;
  {$endif}
end;

destructor TBurnScreenEffect.Destroy;
begin
  BurnRoot.Free;
  inherited;
end;

end.

