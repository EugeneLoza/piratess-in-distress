{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameShipsManager;

{$include gamecompilerconfig.inc}

interface

uses
  Generics.Collections,
  GameGlobal, GameShip;

type
  TShipsManager = class(TObject)
  strict private
  public
    function AllShipsDestroyed: Boolean;
    function AliveShipsCount: Integer;
    function LastShip: TShip;
    function LargestShip: TShip;
    function NoLargeTierShipsAlive: Boolean;
    function HasFlagship: Boolean;
    function TopCovered: Boolean;
    function BottomCovered: Boolean;
  public
    Ships: TShipsList;
    constructor Create; //override;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils, CastleLog;

function TShipsManager.AllShipsDestroyed: Boolean;
var
  S: TShip;
begin
  for S in Ships do
    if S.IsAlive then
      Exit(false);
  Exit(true);
end;

function TShipsManager.AliveShipsCount: Integer;
var
  S: TShip;
begin
  Result := 0;
  for S in Ships do
    if S.IsAlive then
      Inc(Result);
end;

function TShipsManager.LastShip: TShip;
var
  S: TShip;
begin
  Result := nil;
  for S in Ships do
    if S.IsAlive then
      if Result = nil then
        Result := S
      else
        Exit(nil);
end;

function TShipsManager.LargestShip: TShip;
var
  S: TShip;
begin
  Result := nil;
  for S in Ships do
    if S.IsAlive and ((Result = nil) or (Result.Length < S.Length)) then
      Result := S
end;

function TShipsManager.NoLargeTierShipsAlive: Boolean;
var
  S: TShip;
begin
  for S in Ships do
    if (S.Tier >= MinimalLargeShipTier) and S.IsAlive then
      Exit(false);
  Result := true;
end;

function TShipsManager.HasFlagship: Boolean;
var
  S: TShip;
begin
  for S in Ships do
    if (S.Tier >= FlagshipTier) and S.IsAlive then
      Exit(true);
  Result := false;
end;

function TShipsManager.TopCovered: Boolean;
var
  S: TShip;
begin
  for S in Ships do
    if (S.Layer <> nil) and S.Layer.CoversTop then
      Exit(true);
  Result := false;
end;

function TShipsManager.BottomCovered: Boolean;
var
  S: TShip;
begin
  for S in Ships do
    if (S.Layer <> nil) and S.Layer.CoversBottom then
      Exit(true);
  Result := false;
end;

constructor TShipsManager.Create;
begin
  inherited; // parent is empty;
  Ships := TShipsList.Create(true);
end;

destructor TShipsManager.Destroy;
begin
  Ships.Free;
  inherited;
end;

end.

