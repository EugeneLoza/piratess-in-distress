{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameStateMain;

{$include gamecompilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls, CastleControls, CastleKeysMouse,
  GameActress, GameSeaMapInterface, GameRanks, GameQuirks, GameStateBase, GameGlobal;

type
  TStateMain = class(TStateBase)
  strict private
    PlayerHasProps: Integer;
    VictoryAnimation: Boolean;
    ActressSlot: TCastleUserInterface;
    PlayerSeaMap, EnemySeaMap: TSeaMapInterface;
    EnemyQuirks: TCastleHorizontalGroup;
    EnemyShipsHintGroup: TCastleVerticalGroup;
    EnemyAttackDelay: Integer;
    EnemyRushingAttacks: Integer;
    LabelPropsRemaining: TCastleLabel;
    PropRope: TCastleButton;
    PropSkotch: TCastleButton;
    PropScout: TCastleButton;
    PropMine: TCastleButton;
    procedure EnemyQuirksChanged;
    procedure EnemyShipsChanged;
    procedure PlayerHitsMine;
    procedure EnemyHitsMine;
    procedure ClickEnemyMap(const X, Y: Integer);
    procedure ClickRopes(Sender: TObject);
    procedure ClickSkotch(Sender: TObject);
    procedure ClickScout(Sender: TObject);
    procedure ClickMine(Sender: TObject);
    procedure UpdatePropsInfo;
  public
    Actress: TActress;
    procedure PlayerVictory;
    procedure PlayerDefeat;
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Stop; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): boolean; override;
  end;

var
  StateMain: TStateMain;

implementation
uses
  SysUtils,
  CastleComponentSerialize, CastleSoundEngine, CastleLog, CastleUiState,
  GameSounds, GameShip, GameShipsManager, GameFlagship,
  GameStateUpperDeck;

{ TStateMain ----------------------------------------------------------------- }

constructor TStateMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gamestatemain.castle-user-interface';
  DesignPreload := true;
end;

procedure TStateMain.Start;

  function CreatePlayerShips: TShipsManager;
  const
    PlayerShipsDensity = 0.33;
  var
    Rnk, TotalShips, Tier: Integer;
    Ship: TShip;
  begin
    Result := TShipsManager.Create;
    TotalShips := Round(Flagship.PlayerRank * PlayerShipsDensity);
    Rnk := TotalShips;
    repeat
      Ship := TShip.Create;
      Ship.Owner := nil;
      Ship.Layer := nil;
      Tier := Trunc(Rnk / 4) + 1;
      if Tier > 4 then
        Tier := 4;
      Ship.Init(Tier);
      Rnk -= Ship.Length;
      Result.Ships.Add(Ship);
    until Rnk <= 0;
  end;
begin
  inherited;

  Inc(Flagship.Day);

  VictoryAnimation := false;

  PlayerSeaMap := TSeaMapInterface.Create(Self);
  PlayerSeaMap.Actress := nil;
  PlayerSeaMap.Map.SizeX := GetRank(Flagship.PlayerRank).X;
  PlayerSeaMap.Map.SizeY := GetRank(Flagship.PlayerRank).Y;
  PlayerSeaMap.Ships := CreatePlayerShips; // TODO: create them in AddShips and owned by TSeaMap
  PlayerSeaMap.OnMineHit := @EnemyHitsMine;
  PlayerSeaMap.AddShips;
  (DesignedComponent('PlayerMap') as TCastleUserInterface).InsertFront(PlayerSeaMap);
  WriteLnLog('Player Rank = ' + Flagship.PlayerRank.ToString);

  Actress.OnRevealQuirk := @EnemyQuirksChanged;
  Actress.PrepareForBattle;

  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName(Actress.CombatTheme);
  Flagship.LastTrack := Actress.CombatTheme;

  EnemySeaMap := TSeaMapInterface.Create(Self);
  EnemySeaMap.Map.SizeX := GetRank(Actress.Rank).X;
  EnemySeaMap.Map.SizeY := GetRank(Actress.Rank).Y;
  (DesignedComponent('EnemyMap') as TCastleUserInterface).InsertFront(EnemySeaMap);
  EnemySeaMap.Actress := Actress;
  EnemySeaMap.Ships := Actress.Ships; // TODO: create them in AddShips and owned by TSeaMap
  EnemySeaMap.AddShips;
  EnemySeaMap.OnShipDestroyed := @EnemyShipsChanged;
  EnemySeaMap.OnClick := @ClickEnemyMap;
  EnemySeaMap.OnMineHit := @PlayerHitsMine;
  Actress.SetKnotsActive(0);

  (DesignedComponent('ActressName') as TCastleLabel).Caption := Actress.Name;
  ActressSlot := DesignedComponent('ActressSlot') as TCastleUserInterface;
  ActressSlot.InsertFront(Actress.Body);

  EnemyQuirks := DesignedComponent('QuirksGroup') as TCastleHorizontalGroup;
  EnemyQuirksChanged;

  if Actress.HasQuirk('Revealing') then
    Actress.RevealAllQuirks;

  EnemyShipsHintGroup := DesignedComponent('EnemyShipsHintGroup') as TCastleVerticalGroup;
  EnemyShipsChanged;

  PlayerHasProps := Flagship.PropsCount;

  LabelPropsRemaining := DesignedComponent('LabelPropsRemaining') as TCastleLabel;
  PropRope := DesignedComponent('PropRope') as TCastleButton;
  PropRope.OnClick := @ClickRopes;
  PropSkotch := DesignedComponent('PropSkotch') as TCastleButton;
  PropSkotch.OnClick := @ClickSkotch;
  PropScout := DesignedComponent('PropScout') as TCastleButton;
  PropScout.OnClick := @ClickScout;
  PropMine := DesignedComponent('PropMine') as TCastleButton;
  PropMine.OnClick := @ClickMine;
  UpdatePropsInfo;

  if Actress.HasQuirk('Rushing') then
  begin
    EnemyRushingAttacks := Actress.QuirkValueInteger('Rushing');
    //Block input and animate sequentially;
  end else
    EnemyRushingAttacks := 0;

  if Actress.HasQuirk('Lagging') then
    EnemyAttackDelay := Actress.QuirkValueInteger('Lagging')
  else
    EnemyAttackDelay := 0;
end;

procedure TStateMain.Stop;
begin
  Actress.OnRevealQuirk := nil;
  PlayerSeaMap.Ships.Free;
  inherited;
end;

procedure TStateMain.EnemyQuirksChanged;
var
  S: String;
  L: TCastleLabel;
begin
  EnemyQuirks.ClearControls;
  for S in Actress.Quirks.Keys do
    if ((QuirkInfo(S).Kind = qkWild) or (QuirkInfo(S).Kind = qkBoth)) and Actress.QuirkRevealed(S) then
    begin
      L := TCastleLabel.Create(EnemyQuirks);
      L.Caption := Actress.QuirkToString(S);
      EnemyQuirks.InsertFront(L);
    end;
end;

procedure TStateMain.EnemyShipsChanged;

  function ShipImage(const Ship: TShip): TCastleImageControl;
  var
    AliveStr: String;
  begin
    Result := TCastleImageControl.Create(EnemyShipsHintGroup);
    if Ship.IsAlive then
      AliveStr := '-alive'
    else
      AliveStr := '-dead';
    Result.Url := 'castle-data:/ui/ships/ship-' + Ship.OriginalLength.ToString + AliveStr + '.png';
  end;

var
  S: TShip;
  I: Integer;
  TierGroups: array [1..4] of TCastleHorizontalGroup;
begin
  EnemyShipsHintGroup.ClearControls;
  for I := 4 downto 1 do
  begin
    TierGroups[I] := TCastleHorizontalGroup.Create(EnemyShipsHintGroup);
    TierGroups[I].Spacing := 5;
    EnemyShipsHintGroup.InsertFront(TierGroups[I]);
  end;
  for S in Actress.Ships.Ships do
    TierGroups[S.Tier].InsertFront(ShipImage(S));
end;

procedure TStateMain.PlayerHitsMine;
begin
  PlayerSeaMap.KillShip(PlayerSeaMap.Ships.LargestShip);
end;

procedure TStateMain.EnemyHitsMine;
begin
  EnemySeaMap.KillShip(EnemySeaMap.Ships.LargestShip);
end;

procedure TStateMain.ClickEnemyMap(const X, Y: Integer);
begin
  if EnemySeaMap.ShotHere(X, Y) then
  begin
    UpdatePropsInfo;
    if VictoryAnimation then
      Exit;
    if EnemyAttackDelay > 0 then
    begin
      Actress.RevealQuirk('Lagging');
      Dec(EnemyAttackDelay);
      Exit;
    end;
    Actress.DoAi(PlayerSeaMap, EnemySeaMap);
  end;
end;

procedure TStateMain.ClickRopes(Sender: TObject);
begin
  if PlayerHasProps > 0 then
  begin
    Actress.ApplyRope(100, false); // TODO: Player skips turn?
    Dec(PlayerHasProps);
    UpdatePropsInfo;
  end else
    raise Exception.Create('Player clicks prop, but has no props left');
end;

procedure TStateMain.ClickSkotch(Sender: TObject);
begin
  if PlayerHasProps > 0 then
  begin
    Actress.ApplyRope(50, true); // TODO: Player skips turn?
    Dec(PlayerHasProps);
    UpdatePropsInfo;
  end else
    raise Exception.Create('Player clicks prop, but has no props left');
end;

procedure TStateMain.ClickScout(Sender: TObject);
begin
  if (PlayerHasProps > 0) and EnemySeaMap.Map.ShipIsNotVisible(EnemySeaMap.Ships.LargestShip) then
  begin
    EnemySeaMap.RevealLargestShip;
    Dec(PlayerHasProps);
    UpdatePropsInfo;
  end else
    raise Exception.Create('Impossible to use Scout');
end;

procedure TStateMain.ClickMine(Sender: TObject);
begin
  if (PlayerHasProps > 0) and (PlayerSeaMap.Map.CanPutMine) then
  begin
    PlayerSeaMap.AddMine;
    Dec(PlayerHasProps);
    UpdatePropsInfo;
  end else
    raise Exception.Create('Impossible to put mine');
end;

procedure TStateMain.UpdatePropsInfo;
begin
  LabelPropsRemaining.Caption := 'Uses Left : ' + PlayerHasProps.ToString;
  PropRope.Enabled := (PlayerHasProps > 0) and EnemySeaMap.Ships.NoLargeTierShipsAlive;
  PropSkotch.Enabled := (PlayerHasProps > 0) and EnemySeaMap.Ships.NoLargeTierShipsAlive;
  PropMine.Enabled := (PlayerHasProps > 0) and (PlayerSeaMap.Map.CanPutMine);
  PropScout.Enabled := (PlayerHasProps > 0) and EnemySeaMap.Map.ShipIsNotVisible(EnemySeaMap.Ships.LargestShip);
end;

procedure TStateMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  if VictoryAnimation then
    TUiState.Current := StateUpperDeck;
  while EnemyRushingAttacks > 0 do
  begin
    Actress.RevealQuirk('Rushing');
    Actress.DoSillyAi(PlayerSeaMap);
    Dec(EnemyRushingAttacks);
  end;

end;

function TStateMain.Press(const Event: TInputPressRelease): boolean;
begin
  Result := inherited;
  {$IFDEF CHEATS}
  if Event.IsKey(keyF1) then
  begin
    Actress.Captured := true;
    VictoryAnimation := true;
  end;
  {$ENDIF}
end;

procedure TStateMain.PlayerVictory;
begin
  if Actress.IsBound then
    Actress.Captured := true;
  VictoryAnimation := true;
end;

procedure TStateMain.PlayerDefeat;
begin
  VictoryAnimation := true;
end;

end.
