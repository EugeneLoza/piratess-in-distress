{ Copyright (C) 2021-2021 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit TypingLabel;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  CastleControls;

type
  TTypingLabel = class(TCastleLabel)
  private
    FTimer: Single;
    FTypingSpeed: Single;
    function TextLength: Integer;
  public
    procedure ResetText;
    procedure ShowAllText;
    function FinishedTyping: Boolean;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    constructor Create(AOwner: TComponent); override;
  published
    property TypingSpeed: Single read FTypingSpeed write FTypingSpeed default 60; //chars per second
  end;

implementation
uses
  CastleComponentSerialize;

procedure TTypingLabel.Update(const SecondsPassed: Single; var HandleInput: Boolean);
var
  N: Integer;
begin
  inherited;
  FTimer += SecondsPassed;
  N := Round(FTimer * TypingSpeed);
  if N < TextLength then
    MaxDisplayChars := N
  else
    MaxDisplayChars := -1; //show all
end;

function TTypingLabel.TextLength: Integer;
var
  S: String;
begin
  //TODO: Cache it!
  Result := 0;
  for S in Text do
    Result += Length(S);
end;

procedure TTypingLabel.ResetText;
begin
  FTimer := 0;
end;

procedure TTypingLabel.ShowAllText;
begin
  FTimer := 999;
end;

function TTypingLabel.FinishedTyping: Boolean;
begin
  Result := Round(FTimer * TypingSpeed) >= TextLength;
end;

constructor TTypingLabel.Create(AOwner: TComponent);
begin
  inherited;
  FTypingSpeed := 60;
  ResetText;
end;

initialization
  RegisterSerializableComponent(TTypingLabel, 'Typing Label');

end.

