{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameInitialize;

{$include gamecompilerconfig.inc}

interface

procedure SetFullScreen(const AFullScreen: Boolean);
implementation

uses SysUtils,
  CastleApplicationProperties, CastleSoundEngine, CastleConfig,
  CastleWindow, CastleLog, CastleKeysMouse, CastleUIState
  {$region 'Castle Initialization Uses'}
  // The content here may be automatically updated by CGE editor.
  , GameStateMain
  , GameStateContentWarning
  , GameStateMainMenu
  , GameStateUpperDeck
  , GameStateOptions
  {$endregion 'Castle Initialization Uses'},
  GameGlobal, GameRanks, GameSounds, GameQuirks, GameGenerics, GameNamesGenerator;

var
  Window: TCastleWindow;

procedure ApplicationInitialize;
begin
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');

  InitGenerics;
  InitRandom;
  InitNamesGenerator;
  InitRanks;
  InitQuirks;

  UserConfig.Load;
  SetFullScreen(UserConfig.GetValue('fullscreen', false));

  InitializeSounds;

  {$region 'Castle State Creation'}
  // The content here may be automatically updated by CGE editor.
  StateMain := TStateMain.Create(Application);
  StateMainMenu := TStateMainMenu.Create(Application);
  StateUpperDeck := TStateUpperDeck.Create(Application);
  StateOptions := TStateOptions.Create(Application);
  {$endregion 'Castle State Creation'}

  if UserConfig.GetValue('skipcontentwarning', false) then
    TUIState.Current := StateMainMenu
  else
  begin
    StateContentWarning := TStateContentWarning.Create(Application); // don't even create the state otherwise
    TUIState.Current := StateContentWarning;
  end;
end;

procedure SetFullScreen(const AFullScreen: Boolean);
begin
  Window.FullScreen := AFullScreen;
end;

procedure WindowPress(Container: TUIContainer; const Event: TInputPressRelease);
begin
  if Event.Key = keyF11 then
    SetFullScreen(not Window.FullScreen);
  UserConfig.SetValue('fullscreen', Window.FullScreen);
end;

procedure WindowClose(Container: TUIContainer);
begin
  {$IFNDEF Android}
  //SaveGame;
  {$ENDIF}
  Window.Close;
end;

initialization
  WriteLnLog('----------------------------------------------------');
  WriteLnLog(ApplicationProperties.Caption + ' ' + ApplicationProperties.Version);
  WriteLnLog('Copyright (C) 2021-2022 Yevhen Loza');
  WriteLnLog('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.');
  WriteLnLog('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  WriteLnLog('You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.');
  WriteLnLog('----------------------------------------------------');

  Application.OnInitialize := @ApplicationInitialize;

  Window := TCastleWindow.Create(Application);
  Window.ParseParameters; // allows to control window size / fullscreen on the command-line

  {$IFNDEF Android}
    Window.Height := Application.ScreenHeight * 4 div 5;
    Window.Width := Window.Height * 1334 div 750;
    Window.OnPress := @WindowPress;
  {$ENDIF}
  Window.AlphaBits := 8;
  Window.OnCloseQuery := @WindowClose;
  Window.Container.BackgroundEnable := false;

  Application.MainWindow := Window;
end.
