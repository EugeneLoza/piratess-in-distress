{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameQuirks;

{$include gamecompilerconfig.inc}

interface

uses
  Generics.Collections;

type
  TQuirkRevealStatus = (qrHidden, qrRevealPending, qrRevealed);
  TQuirkValue = Integer;

type
  TQuirkKind = (
    { Apply to combat }
    qkWild,
    { Apply to taming, interrogation and tamed bonuses }
    qkCaptured,
    { Applies to both situations }
    qkBoth
  );

type
  TQuirkValueType = (
    qvBoolean,
    qvInteger,
    qvPositiveMultiplier,
    qvNegativeMultiplier,
    qvChance
  );

type
  TQuirkData = class(TObject)
    Value: TQuirkValue;
    Status: TQuirkRevealStatus;
  end;

type
  TQuirkInfo = record
    Name: String;
    Kind: TQuirkKind;
    Tier: Byte;
    ValueType: TQuirkValueType;
    Description: String;
    //MinValue, MaxValue: TQuirkValue; // Todo
    //ValueCurve: TValueCurve; // vcSqrt, vcLinear, vcSqr
    { A quirk this one is incompatible with
      (because they do inverse things }
    Incompatible: String;
    { if this quirk requires other quirks?
      (i.e. can't be the only one) }
    NeedsOtherQuirks: Boolean;
  end;

type
  TQuirksDictionary = specialize TDictionary<String, TQuirkInfo>;

var
  QuirksDictionary: TQuirksDictionary;

procedure InitQuirks;
function QuirkData(const AValue: TQuirkValue): TQuirkData;
function QuirkInfo(const AQuirkName: String): TQuirkInfo;

implementation
uses
  SysUtils,
  CastleLog;

function QuirkData(const AValue: TQuirkValue): TQuirkData;
begin
  if AValue <= 0 then
    WriteLnWarning('Unexpected Value for QuirkData: ' + AValue.ToString);
  Result := TQuirkData.Create;
  Result.Value := AValue;
  Result.Status := qrHidden;
end;

function QuirkInfo(const AQuirkName: String): TQuirkInfo;
begin
  if QuirksDictionary.ContainsKey(AQuirkName) then
    Result := QuirksDictionary[AQuirkName]
  else
    raise Exception.Create('No QuirkInfo available for "' + AQuirkName + '"');
end;

procedure InitQuirks;

  procedure AddQuirkInfo(const AName: String; const AKind: TQuirkKind; const AValueType: TQuirkValueType; const ADescription: String; const ATier: Byte; AIncompatible: String = ''; ANeedsOtherQuirks: Boolean = false);
  var
    Q: TQuirkInfo;
  begin
    Q.Name := AName;
    Q.Kind := AKind;
    Q.ValueType := AValueType;
    Q.Description := ADescription;
    Q.Tier := ATier;
    Q.Incompatible := AIncompatible;
    Q.NeedsOtherQuirks := ANeedsOtherQuirks;
    QuirksDictionary.Add(AName, Q);
  end;
  
  procedure ValidateQuirks;
  var
    S: String;
  begin
    for S in QuirksDictionary.Keys do
      if QuirksDictionary[S].Incompatible <> '' then
      begin
        if S = QuirksDictionary[S].Incompatible then
          raise Exception.Create('Quirk cannot be incompatible with self: ' + S);
        if QuirksDictionary.ContainsKey(QuirksDictionary[S].Incompatible) then
        begin
          if QuirksDictionary[QuirksDictionary[S].Incompatible].Incompatible <> S then
            raise Exception.Create('Incompatible quirks are not paired ' + QuirksDictionary[S].Incompatible + ' and ' + S);
        end else
          raise Exception.Create('Cannot find incompatible quirk ' + QuirksDictionary[S].Incompatible + ' for: ' + S);
      end;
  end;

begin
  QuirksDictionary := TQuirksDictionary.Create;
  AddQuirkInfo('Helpful', qkCaptured, qvPositiveMultiplier, 'Gives a higher boost to rank after tamed.', 0, 'Useless');
  AddQuirkInfo('Useless', qkCaptured, qvNegativeMultiplier, 'Gives a low boost to rank after tamed.', 0, 'Helpful');
  AddQuirkInfo('Shrimped', qkWild, qvInteger, 'Ships are smaller than usual.', 2, 'Elongated');
  AddQuirkInfo('Elongated', qkWild, qvInteger, 'Ships are larger than they should be.', 1, 'Shrimped');
  AddQuirkInfo('Submissive', qkBoth, qvBoolean, 'Reveals the location of her last ship.', 0);
  AddQuirkInfo('Cheating', qkWild, qvChance, 'Doesn''t play fair.', 3); // behavioral perks aren't strictly contradictive to others, they can coexist and it'll still make sense
  AddQuirkInfo('Miss', qkWild, qvChance, 'Misses from time to time, on purpose.', 0);
  AddQuirkInfo('Silly', qkWild, qvChance, 'Doesn''t even know how to play this game.', 0);
  AddQuirkInfo('Skipper', qkWild, qvChance, 'Skips turns sometimes.', 0);
  AddQuirkInfo('Shy', qkBoth, qvBoolean, 'Wants to keep her underwear on no matter what.', 0, 'Shameless');
  AddQuirkInfo('Shameless', qkBoth, qvBoolean, 'Can''t wait to expose more skin than necessary.', 1, 'Shy');
  AddQuirkInfo('Doppelgangster', qkWild, qvChance, 'Can do a double-shot.', 2);
  AddQuirkInfo('Confused', qkWild, qvChance, 'Can accidentally shoot her own field.', 0);
  AddQuirkInfo('Escapist', qkWild, qvPositiveMultiplier, 'Can wriggle out of knots faster.', 2);
  AddQuirkInfo('Entangled', qkWild, qvNegativeMultiplier, 'Has problems getting out of ropes, or maybe just likes it.', 2);
  AddQuirkInfo('Untamed', qkBoth, qvChance, 'Can avoid getting tied up by rope.', 2);
  AddQuirkInfo('Lagging', qkWild, qvInteger, 'Skips a few first turns.', 1);
  AddQuirkInfo('Rushing', qkWild, qvInteger, 'Makes a few shots before the battle starts.', 2);
  AddQuirkInfo('Miner', qkWild, qvInteger, 'Has mines on the field.', 2);

  AddQuirkInfo('Concealing', qkBoth, qvBoolean, 'Hides all other quirks.', 2, 'Revealing', true);
  AddQuirkInfo('Revealing', qkBoth, qvBoolean, 'Reveals all other quirks.', 2, 'Concealing', true);

  // BOSS perks
  AddQuirkInfo('Tutorial', qkBoth, qvBoolean, 'Talks too much.', 0);
  AddQuirkInfo('Ambidext', qkWild, qvBoolean, 'Shoots twice until all large ships are destroyed.', 0);
  AddQuirkInfo('Glitchy', qkWild, qvBoolean, 'Glitches her field hindering your ability to see and attack.', 0);
  AddQuirkInfo('Random', qkWild, qvBoolean, 'Avoids getting localized by rope.', 0);
  AddQuirkInfo('Explosive', qkWild, qvBoolean, 'Has unreasonable amount of mines on the field.', 0);
  AddQuirkInfo('Tempting', qkWild, qvBoolean, 'Your ships cannot resist her call and may pass on her side.', 0); // UNIMPLEMENTED
  AddQuirkInfo('Clairvoyant', qkWild, qvBoolean, 'Knows where your ships are until all large ships are destroyed.', 0);
  AddQuirkInfo('Enchanting', qkWild, qvBoolean, 'Maxes out your rank.', 0);
  AddQuirkInfo('Unbeatable', qkWild, qvBoolean, 'Small ships cannot be attacked unltil large ships are destroyed.', 0); // UNIMPLEMENTED
  AddQuirkInfo('Unyielding', qkWild, qvBoolean, 'Cannot be bound by rope.', 0);
  AddQuirkInfo('Fierce', qkWild, qvBoolean, 'Shoots twice until the flagship is destroyed.', 0);

  ValidateQuirks;
end;

procedure FreeQuirks;
begin
  QuirksDictionary.Free;
end;

finalization
  FreeQuirks;
end.

