{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameActressPose;

{$include gamecompilerconfig.inc}

interface

uses
  Generics.Collections,
  CastleImages, CastleControls, CastleUiControls,
  GameGlobal;

type
  TActressLayer = class(TObject)
  strict private
    function GetActive: Boolean;
    procedure SetActive(const Value: Boolean);
  public
    Image: TCastleImage;
    ImageControl: TCastleImageControl;
    Tier: Integer;
    CoversTop: Boolean;
    CoversBottom: Boolean;
    Knots: Boolean;
    OnClothesRemove: TSimpleProcedure;
    function IsUnderwear: Boolean;
    property IsActive: Boolean read GetActive write SetActive;
    destructor Destroy; override;
  end;
  TActressLayerList = specialize TObjectList<TActressLayer>;

type
  TActressPose = class(TObject)
  strict private
    procedure OnClothesRemoved;
    procedure ActivateLayers;
  public
    Layers: TActressLayerList;
    Pose: TCastleUserInterface;
    BottomCovered, TopCovered: Boolean;
    procedure GetCoverStatus;
    constructor Create; //override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleLog;

procedure TActressLayer.SetActive(const Value: Boolean);
begin
  ImageControl.Exists := Value;
  if not Value and Assigned(OnClothesRemove) then
    OnClothesRemove;
end;
function TActressLayer.GetActive: Boolean;
begin
  Result := ImageControl.Exists;
end;

function TActressLayer.IsUnderwear: Boolean;
begin
  Result := (Tier <= 2) and (CoversTop or CoversBottom);
end;

destructor TActressLayer.Destroy;
begin
  Image.Free;
  ImageControl.Free;
  inherited;
end;

procedure TActressPose.ActivateLayers({const Array of Integer});
var
  I: Integer;
begin
  for I := 0 to Pred(Layers.Count) do
    Layers[I].IsActive := true;
end;

procedure TActressPose.GetCoverStatus;
var
  I: Integer;
begin
  BottomCovered := false;
  TopCovered := false;
  for I := 0 to Pred(Layers.Count) do
    if Layers[I].IsActive then
    begin
      if Layers[I].CoversBottom then
        BottomCovered := true;
      if Layers[I].CoversTop then
        TopCovered := true;
    end;
end;

procedure TActressPose.OnClothesRemoved;
var
  BottomOld, TopOld: Boolean;
begin
  BottomOld := BottomCovered;
  TopOld := TopCovered;
  GetCoverStatus;
  if BottomOld and not BottomCovered then
    WriteLnLog('Bottom removed');
  if TopOld and not TopCovered then
    WriteLnLog('Top removed');
  if (BottomOld or TopOld) and not BottomCovered and not TopCovered then
    WriteLnLog('Undressed');
end;

constructor TActressPose.Create;

  function NewLayer(const AUrl: String; const ATier: Integer; const ACoversTop, ACoversBottom: Boolean; AKnots: Boolean = false): TActressLayer;
  begin
    NewLayer := TActressLayer.Create;
    NewLayer.Image := LoadImage(AUrl, [TRGBAlphaImage]);
    NewLayer.Tier := ATier;
    NewLayer.CoversTop := ACoversTop;
    NewLayer.CoversBottom := ACoversBottom;
    NewLayer.Knots := AKnots;
    NewLayer.OnClothesRemove := @OnClothesRemoved;
    NewLayer.ImageControl := TCastleImageControl.Create(nil);
    NewLayer.ImageControl.Image := NewLayer.Image;
    NewLayer.ImageControl.OwnsImage := false;
    NewLayer.ImageControl.Stretch := true;
    NewLayer.ImageControl.HeightFraction := 1.0;
    NewLayer.ImageControl.VerticalAnchorParent := vpMiddle;
    NewLayer.ImageControl.VerticalAnchorSelf := vpMiddle;
    NewLayer.ImageControl.HorizontalAnchorParent := hpMiddle;
    NewLayer.ImageControl.HorizontalAnchorSelf := hpMiddle;
    NewLayer.ImageControl.ProportionalScaling := psEnclose;
    Layers.Add(NewLayer);
  end;

var
  L: TActressLayer;
begin
  inherited; // Empty
  Layers := TActressLayerList.Create(true);
  NewLayer('castle-data:/actresses/cornee/body.png', 0, false, false);
  NewLayer('castle-data:/actresses/cornee/bottom.png', 1, false, true);
  NewLayer('castle-data:/actresses/cornee/top.png', 1, true, false);
  NewLayer('castle-data:/actresses/cornee/amulet.png', 1, false, false);
  NewLayer('castle-data:/actresses/cornee/earrings.png', 1, false, false);
  NewLayer('castle-data:/actresses/cornee/boots.png', 2, false, false);
  NewLayer('castle-data:/actresses/cornee/bowtie.png', 2, false, false);
  NewLayer('castle-data:/actresses/cornee/bracelets.png', 2, false, false);
  NewLayer('castle-data:/actresses/cornee/knots.png', 0, false, false, true); // knots layer
  NewLayer('castle-data:/actresses/cornee/skirt.png', 3, false, true);
  NewLayer('castle-data:/actresses/cornee/bluse.png', 3, true, false);
  NewLayer('castle-data:/actresses/cornee/overcoat.png', 4, true, true);
  Pose := TCastleUserInterface.Create(nil);
  Pose.FullSize := true;
  for L in Layers do
    Pose.InsertFront(L.ImageControl);
  GetCoverStatus;
end;

destructor TActressPose.Destroy;
begin
  Layers.Free;
  Pose.Free;
  inherited;
end;

end.

