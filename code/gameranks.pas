{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameRanks;

{$include gamecompilerconfig.inc}

interface

uses
  Generics.Collections;

const
  MinSide = 4;
  MaxSide = 12;
  MaxDiff = 3;

type
  TRank = record
    X, Y, Value: Integer;
  end;
  TRanks = specialize TDictionary<Integer, TRank>;

procedure InitRanks;
function GetRank(const ARank: Single): TRank;
function MaxRank: TRank;
function MinRank: TRank;
implementation
uses
  SysUtils,
  CastleLog,
  GameGenerics;

var
  Ranks: TRanks;

function GetRank(const ARank: Single): TRank;
var
  CurrentRank: Integer;
  R: TRank;
begin
  CurrentRank := -1;
  for R in Ranks.Values do
    if (R.Value > CurrentRank) and (R.Value <= Round(ARank)) then
    begin
      CurrentRank := R.Value;
      Result := R;
    end;
  if CurrentRank <= 0 then
    raise Exception.Create('Cannot determine physical rank for rank = ' + ARank.ToString);
end;

function RankRec(const Xx, Yy: Byte): TRank;
begin
  Result.X := Xx;
  Result.Y := Yy;
  Result.Value := Xx * Yy;
end;

function MaxRank: TRank;
begin
  Result := RankRec(MaxSide + 1, MaxSide + 1);
end;
function MinRank: TRank;
begin
  Result := RankRec(MinSide, MinSide);
end;

procedure InitRanks;
var
  Ix, Iy: Integer;
  List: specialize TList<Single>;
  R: TRank;
begin
  Ranks := TRanks.Create;
  for Iy := MinSide to MaxSide do
    for Ix := Iy to MaxSide do
      if Ix - Iy <= MaxDiff then
      begin
        if not Ranks.ContainsKey(Ix * Iy) then
          Ranks.Add(Ix * Iy, RankRec(Ix, Iy))
        else
        if Ix - Iy < Ranks[Ix * Iy].X - Ranks[Ix * Iy].Y then
          Ranks.AddOrSetValue(Ix * Iy, RankRec(Ix, Iy));
      end;
  Ranks.Add(MaxRank.Value, MaxRank); // Add final boss
  List := (specialize TList<Single>).Create;
  for R in Ranks.Values do
    List.Add(R.Value);
  List.Sort(ComparerSingleAscending);
  Iy := 16;
  for Ix := 0 to Pred(List.Count) do
  begin
    WriteLnLog(List[Ix].ToString, '(+' + (List[Ix] - Iy).ToString() + ')');
    Iy := Round(List[Ix]);
  end;
  WriteLnLog('Total ranks', List.Count.ToString);
  List.Free;
end;

procedure FreeRanks;
begin
  Ranks.Free;
end;

finalization
  FreeRanks;
end.
