{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameStateBase;

{$include gamecompilerconfig.inc}

interface

uses
  CastleUIState;

type
  TStateBase = class(TUIState)
  public
    procedure Start; override;
  end;

implementation
uses
  CastleComponentSerialize, CastleUiControls,
  GameScreenEffect;

procedure TStateBase.Start;
begin
  inherited;
  ApplyEffect(DesignedComponent('GameContainer') as TCastleUserInterface);
end;

end.

