{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameSeaMap;

{$include gamecompilerconfig.inc}

interface

uses
  GameShip, GameGlobal, GameRanks;

type
  TMapTile = (mtOutOfBounds, mtUndefined, mtMiss, mtEmpty, mtShipHit, mtShipAlive,
              mtMine, mtMineHit);

type
  TSeaMap = class(TObject)
  strict private
    procedure MarkMapSafe(const AX, AY: Integer);
  public
    GlitchMap: array[0..MaxSide, 0..MaxSide] of Boolean;
    Vis: array[0..MaxSide, 0..MaxSide] of Boolean;
    Map: array [0..MaxSide, 0..MaxSide] of TMapTile;
    SizeX, SizeY: Integer;
    function CanShootHere(const AX, AY: Integer): Boolean;
    procedure Clear;
    function CanPutMine: Boolean;
    function CanPutMineHere(const AX, AY: Integer): Boolean;
    procedure ApplyGlitch(const GlitchChance: Single);
    procedure MarkShip(Ship: TShip);
    { If at least one tile of the ship is not visible.
      Makes sense only for Piratess' ships as ships are always visible otherwise }
    function ShipIsNotVisible(Ship: TShip): Boolean;
    procedure MarkShipKilled(Ship: TShip);
    procedure WriteShipToMap(const AShip: TShip);
    function CanPutShipHere(const AShip: TShip): Boolean;
    function ReadMapSafe(const AX, AY: Integer): TMapTile;
    procedure WriteMapSafe(const AX, AY: Integer; const AValue: TMapTile);
  end;

function MapTileToString(const AMapTile: TMapTile): String;
implementation
uses
  SysUtils, CastleLog, typinfo,
  GameAi;

{$WARN 6018 off : Unreachable code}
function TSeaMap.CanShootHere(const AX, AY: Integer): Boolean;
begin
  case ReadMapSafe(AX, AY) of
    mtOutOfBounds: Result := false;
    mtUndefined: Result := true;
    mtMiss: Result := false;
    mtEmpty: Result := false;
    mtShipHit: Result := false;
    mtShipAlive: Result := true;
    mtMine: Result := true;
    mtMineHit: Result := false;
    else
      raise Exception.Create('Unexpected MapTile: ' + MapTileToString(ReadMapSafe(AX, AY)));
  end;
end;
{$WARN 6018 on}

procedure TSeaMap.Clear;
var
  IX, IY: Integer;
begin
  for IX := 0 to Pred(SizeX) do
    for IY := 0 to Pred(SizeY) do
    begin
      Map[IX, IY] := mtUndefined;
      Vis[IX, IY] := false;
    end;
end;

function TSeaMap.CanPutMineHere(const AX, AY: Integer): Boolean;

  function IsNotShip(const MX, MY: Integer): Boolean;
  begin
    Result := (ReadMapSafe(MX, MY) <> mtShipAlive) and (ReadMapSafe(MX, MY) <> mtShipHit);
  end;

begin
  if ReadMapSafe(AX, AY) = mtUndefined then
    Result := IsNotShip(AX + 1, AY) and IsNotShip(AX - 1, AY) and
              IsNotShip(AX, AY + 1) and IsNotShip(AX, AY - 1)
  else
    Result := false;
end;

function TSeaMap.CanPutMine: Boolean;
var
  IX, IY: Integer;
begin
  for IX := 0 to Pred(SizeX) do
    for IY := 0 to Pred(SizeY) do
      if CanPutMineHere(IX, IY) then
        Exit(true);
  Result := false;
end;

procedure TSeaMap.ApplyGlitch(const GlitchChance: Single);
var
  ShootableTiles: TMovesList;
  K, M, IX, IY: Integer;
begin
  for IX := 0 to Pred(SizeX) do
    for IY := 0 to Pred(SizeY) do
      GlitchMap[IX, IY] := Rnd.Random < GlitchChance;

  ShootableTiles := SillyAi(Self); // this returns all shootable tiles for this SeaMap
  // Make approximately "minimum" half of the shootable tiles non-glitching
  for K := 0 to ShootableTiles.Count div 2 + 1 do
  begin
    M := Rnd.Random(ShootableTiles.Count); // note that we can repeat tiles here, but we still always have at least one move
    GlitchMap[ShootableTiles[M].X, ShootableTiles[M].Y] := false;
  end;
  ShootableTiles.Free;
end;

function TSeaMap.ReadMapSafe(const AX, AY: Integer): TMapTile;
begin
  if (AX >= 0) and (AY >= 0) and (AX < SizeX) and (AY < SizeY) then
    Result := Map[AX, AY]
  else
    Result := mtOutOfBounds;
end;

procedure TSeaMap.WriteMapSafe(const AX, AY: Integer; const AValue: TMapTile);
begin
  if (AX >= 0) and (AY >= 0) and (AX < SizeX) and (AY < SizeY) then
    if Map[AX, AY] = mtUndefined then
      Map[AX, AY] := AValue;
end;

procedure TSeaMap.MarkMapSafe(const AX, AY: Integer);
begin
  if (AX >= 0) and (AY >= 0) and (AX < SizeX) and (AY < SizeY) then
  begin
    Vis[AX, AY] := true;
    if Map[AX, AY] = mtUndefined then
      Map[AX, AY] := mtEmpty;
  end;
end;

function TSeaMap.CanPutShipHere(const AShip: TShip): Boolean;
var
  J: Integer;
begin
  for J := 0 to Pred(AShip.Length) do
    if ReadMapSafe(AShip.X + AShip.DX * J, AShip.Y + AShip.DY * J) <> mtUndefined then
      Exit(false);
  Result := true;
end;

procedure TSeaMap.MarkShip(Ship: TShip);
var
  I: Integer;
  X, Y: Integer;
begin
  if Ship = nil then
    Exit;
  for I := 0 to Pred(Ship.Length) do
  begin
    X := Ship.X + Ship.DX * I;
    Y := Ship.Y + Ship.DY * I;
    {$ifdef ShipsCannotTouchCorners}
    MarkMapSafe(X + 1, Y + 1);
    MarkMapSafe(X + 1, Y - 1);
    MarkMapSafe(X - 1, Y + 1);
    MarkMapSafe(X - 1, Y - 1);
    {$endif}
    MarkMapSafe(X - 1, Y    );
    MarkMapSafe(X + 1, Y    );
    MarkMapSafe(X    , Y + 1);
    MarkMapSafe(X    , Y - 1);
  end;
end;

function TSeaMap.ShipIsNotVisible(Ship: TShip): Boolean;
var
  I: Integer;
begin
  if Ship = nil then
    Exit(false); // if no ship is provided, it cannot be discovered, this can legally happen if no ships are alive
  for I := 0 to Pred(Ship.Length) do
    if not Vis[Ship.X + Ship.Dx * I, Ship.Y + Ship.Dy * I] then
      Exit(true);
  Result := false;
end;

procedure TSeaMap.MarkShipKilled(Ship: TShip);
var
  I: Integer;
begin
  for I := 0 to Pred(Ship.Length) do
    Map[Ship.X + Ship.Dx * I, Ship.Y + Ship.Dy * I] := mtShipHit;
  MarkShip(Ship);
end;

procedure TSeaMap.WriteShipToMap(const AShip: TShip);
var
  I: Integer;
  X, Y: Integer;
begin
  for I := 0 to Pred(AShip.Length) do
  begin
    X := AShip.X + AShip.DX * I;
    Y := AShip.Y + AShip.DY * I;
    Map[X, Y] := mtShipAlive;
    {$ifdef ShipsCannotTouchCorners}
    WriteMapSafe(X + 1, Y + 1, mtEmpty);
    WriteMapSafe(X + 1, Y - 1, mtEmpty);
    WriteMapSafe(X - 1, Y + 1, mtEmpty);
    WriteMapSafe(X - 1, Y - 1, mtEmpty);
    {$endif}
    WriteMapSafe(X + 1, Y    , mtEmpty);
    WriteMapSafe(X - 1, Y    , mtEmpty);
    WriteMapSafe(X    , Y + 1, mtEmpty);
    WriteMapSafe(X    , Y - 1, mtEmpty);
  end;
end;

function MapTileToString(const AMapTile: TMapTile): String;
begin
  Result := GetEnumName(TypeInfo(TMapTile), Ord(AMapTile));
end;

end.

