{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameNamesGenerator;

{$include gamecompilerconfig.inc}

interface

function GenerateRandomName: String;

procedure InitNamesGenerator;
implementation
uses
  SysUtils,
  CastleLog,
  GameGlobal;

const
  FirstSyllable: array [0..125] of String = (
    'Accou',
    'Acte',
    'Aime',
    'Allo',
    'Anto',
    'Arma',
    'Attoi',
    'Ava',
    'Bate',
    'Berna',
    'Bime',
    'Blana',
    'Cami',
    'Cata',
    'Cha',
    'Chlo',
    'Clai',
    'Clau',
    'Cleme',
    'Cloe',
    'Coma',
    'Cori',
    'Cou',
    'Dale',
    'Danie',
    'Dece',
    'Deni',
    'Densi',
    'Desi',
    'Deta',
    'Deu',
    'Domini',
    'Elai',
    'Ela',
    'Empo',
    'Ete',
    'Exi',
    'Fai',
    'Falto',
    'Feui',
    'Fleu',
    'Frede',
    'Garre',
    'Gasto',
    'Gene',
    'Grede',
    'Gilbe',
    'Gise',
    'Ili',
    'Ince',
    'Ima',
    'Jacque',
    'Jea',
    'Jewe',
    'Joli',
    'Jose',
    'Juli',
    'Lati',
    'Leau',
    'Leo',
    'Lorai',
    'Loui',
    'Luci',
    'Madelei',
    'Malo',
    'Mano',
    'Mara',
    'Marce',
    'Margo',
    'Mari',
    'Marti',
    'Mauri',
    'Miche',
    'Mise',
    'Moi',
    'Moni',
    'Mona',
    'Nai',
    'Navi',
    'Neu',
    'Nico',
    'Noe',
    'Oce',
    'Ode',
    'Oli',
    'Orvi',
    'Parcou',
    'Pasca',
    'Pase',
    'Patri',
    'Pau',
    'Pauli',
    'Pavilo',
    'Perci',
    'Phili',
    'Pie',
    'Puna',
    'Qua',
    'Quenti',
    'Rapha',
    'Recue',
    'Rame',
    'Richa',
    'Rou',
    'Ruse',
    'Sacha',
    'Sai',
    'Samu',
    'Seba',
    'Simo',
    'Sinclai',
    'Suze',
    'Theo',
    'Thoma',
    'Tra',
    'Travi',
    'Treso',
    'Trista',
    'Vale',
    'Victo',
    'Vie',
    'Viole',
    'Viva',
    'Voya',
    'Yvo',
    'Zoe'
  );
  LastSyllable: array [0..33] of String = (
    're',
    'rke',
    'rte',
    'rse',
    'rde',
    'rpe',
    'rge',
    'rle',
    'rme',
    'rfe',
    'rve',
    'se',
    'sse',
    'te',
    'tte',
    'tre',
    'che',
    'de',
    'dre',
    'ne',
    'nne',
    'ppe',
    'se',
    'fe',
    'fre',
    'ge',
    'gre',
    'ke',
    'kre',
    'le',
    'lle',
    've',
    'me',
    'mre'
  );

var
  FirstSyllableStatistics: array of Integer;
  LastSyllableStatistics: array of Integer;
  NameSpace: array of String;

procedure InitNamesGenerator;
begin
  SetLength(FirstSyllableStatistics, Length(FirstSyllable));
  SetLength(LastSyllableStatistics, Length(LastSyllable));
  WriteLnLog('Names initialized: ' + IntToStr(Length(FirstSyllable) * Length(LastSyllable)) +
    ' possible combinations');
end;

function GenerateRandomName: String;

  function NotInNameSpace(const aName: String): Boolean;
  var
    S: String;
  begin
    for S in NameSpace do
      if S = aName then
        Exit(false);
    Result := true;
  end;

var
  I: Integer;
  MinFirst, MinLast: Integer;
  First, Last: Integer;
begin
  repeat
    MinFirst := MaxInt;
    for I := 0 to Pred(Length(FirstSyllableStatistics)) do
      if FirstSyllableStatistics[I] < MinFirst then
        MinFirst := FirstSyllableStatistics[I];
    repeat
      First := Rnd.Random(Length(FirstSyllable));
    until (FirstSyllableStatistics[First] = MinFirst) or (Rnd.Random < 0.01);

    MinLast := MaxInt;
    for I := 0 to Pred(Length(LastSyllableStatistics)) do
      if LastSyllableStatistics[I] < MinLast then
        MinLast := LastSyllableStatistics[I];
    repeat
      Last := Rnd.Random(Length(LastSyllable));
    until (LastSyllableStatistics[Last] = MinLast) or (Rnd.Random < 0.01);

    Result := FirstSyllable[First] + LastSyllable[Last];
  until NotInNamespace(Result); // Total: > 2k names. Theoretically Player can't generate that much

  SetLength(Namespace, Length(Namespace) + 1);
  NameSpace[Length(Namespace) - 1] := Result;
  FirstSyllableStatistics[First] += 1;
  LastSyllableStatistics[Last] += 1;
end;

end.

