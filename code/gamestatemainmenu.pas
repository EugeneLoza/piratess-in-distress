{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameStateMainMenu;

{$include gamecompilerconfig.inc}

interface

uses
  Classes,
  CastleVectors, CastleUIState, CastleUIControls, CastleControls, CastleKeysMouse,
  GameStateBase;

type
  TStateMainMenu = class(TStateBase)
  strict private
    procedure ClickContinue(Sender: TObject);
    procedure ClickNewCampaign(Sender: TObject);
    procedure ClickAchievements(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickCredits(Sender: TObject);
    procedure ClickQuit(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateMainMenu: TStateMainMenu;

implementation
uses
  SysUtils,
  CastleApplicationProperties, CastleWindow, CastleSoundEngine,
  GameSounds, GameFlagship, GameActress,
  GameStateMain, GameStateOptions,
  GameGlobal;

constructor TStateMainMenu.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/mainmenu.castle-user-interface';
end;

procedure TStateMainMenu.Start;
var
  ButtonContinue, ButtonNewCamapign, ButtonQuit: TCastleButton;
begin
  inherited;

  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('menu_music');

  ButtonContinue := DesignedComponent('ButtonContinue') as TCastleButton;
  ButtonContinue.Exists := false; // todo: Save game exists
  ButtonContinue.OnClick := @ClickContinue;
  ButtonNewCamapign := DesignedComponent('ButtonNewCamapign') as TCastleButton;
  ButtonNewCamapign.Exists := not ButtonContinue.Exists;
  ButtonNewCamapign.OnClick := @ClickNewCampaign;
  ButtonQuit := DesignedComponent('ButtonQuit') as TCastleButton;
  ButtonQuit.Exists := ApplicationProperties.ShowUserInterfaceToQuit;
  ButtonQuit.OnClick := @ClickQuit;

  (DesignedComponent('ButtonAchievements') as TCastleButton).OnClick := @ClickAchievements;
  (DesignedComponent('ButtonOptions') as TCastleButton).OnClick := @ClickOptions;
  (DesignedComponent('ButtonCredits') as TCastleButton).OnClick := @ClickCredits;
end;

procedure TStateMainMenu.ClickContinue(Sender: TObject);
begin
  SoundClickForward;
  //TODO
end;

procedure TStateMainMenu.ClickNewCampaign(Sender: TObject);

  procedure StartNewGame;
  var
    Actress: TActress;
  begin
    FreeAndNil(Flagship);
    Flagship := TFlagship.Create;
    Flagship.PlayerBaseRank := 16;

    Actress := TActress.Create;
    Actress.Init('Lapine', 16);
    Actress.UnlocksTier := 1;
    Actress.CombatTheme := 'secretary_battle';
    Actress.InterrogationTheme := 'secretary_battle';
    Actress.Quirks.Clear;
    Actress.AddQuirk('Helpful', 50);
    Actress.AddQuirk('Submissive', 1);
    Actress.AddQuirk('Tutorial', 1);
    Flagship.ActressesDictionary.Add(Actress.Id, Actress);
    StateMain.Actress := Actress; // Temporary
  end;

begin
  SoundClickForward;
  //Sound Click new Game
  StartNewGame;
  //StateMain.Actress := Flagship.ActressesList[0]; // := Secretary;
  TUIState.Current := StateMain; // := StateTutorialStart;
  // or StateUpperDeck - show popup with "Normal Campaign (Recommend)" and "Random Game"
end;

procedure TStateMainMenu.ClickAchievements(Sender: TObject);
begin
  SoundClickForward;
  //TODO
end;

procedure TStateMainMenu.ClickOptions(Sender: TObject);
begin
  SoundClickForward;
  TUiState.Push(StateOptions);
end;

procedure TStateMainMenu.ClickCredits(Sender: TObject);
begin
  SoundClickForward;
  //TODO
end;

procedure TStateMainMenu.ClickQuit(Sender: TObject);
begin
  SoundClickBack;
  Application.MainWindow.Close;
end;

end.
