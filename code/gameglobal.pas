{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameGlobal;

{$include gamecompilerconfig.inc}

interface
uses
  CastleRandom;

type
  TSimpleProcedure = procedure of object;

var
  Rnd: TCastleRandom;

function FloatToStringRounded(const AValue: Single; const ADigits: Byte): String;
procedure InitRandom;

implementation
uses
  SysUtils;

procedure InitRandom;
begin
  Rnd := TCastleRandom.Create;
end;

function FloatToStringRounded(const AValue: Single; const ADigits: Byte): String;

  function TenPower(const APower: Byte): Single;
  var
    J: Integer;
  begin
    Result := 1;
    for J := 1 to APower do
      Result *= 10;
  end;

var
  WorkValue: Single;
  V: Single;
  I: Integer;
begin
  if ADigits > 0 then
  begin
    WorkValue := Round(AValue * TenPower(ADigits)) / TenPower(ADigits); // Properly round the digits - this value can contain floating-point error
    Result := Trunc(WorkValue).ToString + '.';
    V := WorkValue - Trunc(WorkValue);
    for I := 0 to Pred(ADigits) do
    begin
      V *= 10;
      Result += Trunc(V).ToString;
      V := V - Trunc(V);
    end;
  end else
    Result := Round(AValue).ToString
end;

finalization
  Rnd.Free;
end.

