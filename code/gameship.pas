{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameShip;

{$include gamecompilerconfig.inc}

interface
uses
  Generics.Collections,
  {temporary} GameActressPose,
  GameGlobal;

const
  MinimalLargeShipTier = 3;
  MaxPossibleShipLength = 9;
  NormalShipHp = 10;
  AttackDamage = NormalShipHp;
  FlagshipTier = 4; //5

type
  TShip = class(TObject)
  strict private
    FTier: Integer;
    function GetLength: Integer;
    function GetOriginalLength: Integer;
    function DefaultHp: Integer;
    procedure Die;
  public
    Alive: array[0..MaxPossibleShipLength - 1] of Integer; // TODO: Convert to something more reasonable (dynamic array with ShipLength?)
    Owner: TObject;
    X, Y: Byte;
    Dx, Dy: ShortInt;
    Layer: TActressLayer;
    function IsLarge: Boolean;
    property Tier: Integer read FTier;
    { Physical length of the ship (the one on the Map) }
    property Length: Integer read GetLength;
    { Length unaffected by Quirks (the one in the UI) }
    property OriginalLength: Integer read GetOriginalLength;
    procedure Init(const ATier: Byte);
    function IsHealthy: Boolean;
    function IsAlive: Boolean;
    function IsHere(const Ax, Ay: Byte): Boolean;
    function HitHere(const Ax, Ay: Byte): Boolean;
    procedure Kill;
  end;
  TShipsList = specialize TObjectList<TShip>;

implementation
uses
  SysUtils,
  CastleLog,
  GameSounds, GameActress;

procedure TShip.Init(const ATier: Byte);
var
  I: Integer;
begin
  FTier := ATier;
  for I := 0 to Pred(Length) do
    Alive[I] := DefaultHp;
end;

function TShip.IsLarge: Boolean;
begin
  Result := Tier >= MinimalLargeShipTier;
end;

function TShip.GetLength: Integer;
begin
  if (Owner <> nil) and (Owner as TActress).HasQuirk('Shrimped') then
  begin
    Result := OriginalLength - (Owner as TActress).QuirkValueInteger('Shrimped');
    if Result <= 0 then
      Result := 1;
  end else
  if (Owner <> nil) and (Owner as TActress).HasQuirk('Elongated') then
  begin
    Result := OriginalLength + (Owner as TActress).QuirkValueInteger('Elongated');
  end else
    Result := OriginalLength;
end;

function TShip.GetOriginalLength: Integer;
begin
  Result := FTier + 1;
end;

function TShip.DefaultHp: Integer;
begin
  // if owner.HasQuirk('Healthy');
  Result := NormalShipHp;
end;

function TShip.IsHere(const Ax, Ay: Byte): Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Length) do
    if (Alive[I] > 0) and (Ax = I * Dx + X) and (Ay = I * Dy + Y) then
      Exit(true);
  Result := false;
end;

function TShip.HitHere(const Ax, Ay: Byte): Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Length) do
    if (Alive[I] > 0) and (Ax = I * Dx + X) and (Ay = I * Dy + Y) then
    begin
      Alive[I] -= AttackDamage;
      if not IsAlive then // ship destroyed
        Die;
      Exit(true);
    end;
  Result := false;
end;

procedure TShip.Kill;
var
  I: Integer;
begin
  for I := 0 to Pred(Length) do
    Alive[I] := 0;
  Die;
end;

procedure TShip.Die;
var
  S, STop: TShip;
  L: TActressLayer;
  SList: TShipsList;
begin
  if Owner <> nil then
  begin
    if (Owner as TActress).HasQuirk('Shrimped') and (Length < OriginalLength) then
      (Owner as TActress).RevealQuirk('Shrimped');
    if (Owner as TActress).HasQuirk('Elongated') and (Length > OriginalLength) then
      (Owner as TActress).RevealQuirk('Elongated');
  end;
  if Layer <> nil then
  begin
    //Shy quirk
    if (Owner as TActress).HasQuirk('Shy') and (Layer.CoversBottom or Layer.CoversTop) then
    begin
      SList := TShipsList.Create(false);
      STop := nil;
      for S in (Owner as TActress).Ships.Ships do
        if S.IsAlive and (S.IsLarge = IsLarge) then //this ship is already not IsAlive
        begin
          if not (S.Layer.CoversBottom or S.Layer.CoversTop) then
            SList.Add(S);
          if S.Layer.CoversTop then
            STop := S;
        end;
      if SList.Count > 0 then
      begin
        (Owner as TActress).RevealQuirk('Shy');
        S := SList[Rnd.Random(SList.Count)];
        L := Layer;
        Layer := S.Layer;
        S.Layer := L;
      end else
      if Layer.CoversBottom and (STop <> nil) then
      begin
        (Owner as TActress).RevealQuirk('Shy');
        L := Layer;
        Layer := STop.Layer;
        STop.Layer := L;
      end; //else fail.
      SList.Free;
    end;
    //Shameless quirk
    if (Owner as TActress).HasQuirk('Shameless') and not Layer.CoversBottom and not Layer.CoversTop then
    begin
      SList := TShipsList.Create(false);
      for S in (Owner as TActress).Ships.Ships do
        if S.IsAlive and (S.IsLarge = IsLarge) then //this ship is already not IsAlive
          if S.Layer.CoversBottom or S.Layer.CoversTop then
            SList.Add(S);
      if SList.Count > 0 then
      begin
        (Owner as TActress).RevealQuirk('Shameless');
        S := SList[Rnd.Random(SList.Count)];
        L := Layer;
        Layer := S.Layer;
        S.Layer := L;
      end; //else fail
      SList.Free;
    end;
    {todo}
    Layer.IsActive := false;
    // TODO: This should be in Layer, not here
    case Layer.Tier of
      4,5: SoundRipLong;
      2,3: SoundRipMedium;
      1: SoundRipShort;
    end;
  end;
end;

function TShip.IsHealthy: Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Length) do
    if Alive[I] = DefaultHp then
      Exit(false);
  Result := true;
end;

function TShip.IsAlive: Boolean;
var
  I: Integer;
begin
  for I := 0 to Pred(Length) do
    if Alive[I] > 0 then
      Exit(true);
  Result := false;
end;

end.
