{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameSeaMapInterface;

{$include gamecompilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls, CastleControls, CastleGlImages, CastleKeysMouse,
  GameShip, GameShipsManager, GameActress, GameGlobal, GameRanks, GameSeaMap;

type
  TMapClick = procedure(const X, Y: Integer) of object;

type
  TSeaMapInterface = class(TCastleUserInterface)
  strict private
    FirstRender: Boolean;
    StartX, StartY: Single;
    Scale: Single;
    function IsPlayer: Boolean;
    procedure ProcessShipKilled(const AShip: TShip);
  public
    Map: TSeaMap;
    Texture: TDrawableImage;
    Ships: TShipsManager;
    Actress: TActress;
    OnShipDestroyed: TSimpleProcedure;
    OnClick: TMapClick;
    OnMineHit: TSimpleProcedure;
    procedure AddMine;
    procedure RevealLargestShip;
    procedure KillShip(const AShip: TShip);
    procedure AddShips;
    { Shots at this map tile; Returns:
      true: shot is valid and was executed;
      false: shot is invalid, nothing changed; }
    function ShotHere(const Mx, My: Integer): Boolean;
  public
    procedure Render; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils, Generics.Collections,
  CastleLog, CastleRectangles,
  GameSounds, GameAi,
  GameStateMain;

type
  TAnchorPoint = record
    X, Y: Byte;
    DX, DY: ShortInt;
  end;
  TAnchorList = specialize TList<TAnchorPoint>;

procedure TSeaMapInterface.AddShips;
var
  AnchorList: TAnchorList;

  procedure InitAnchorList;
  var
    JX, JY: Integer;
    A: TAnchorPoint;

    procedure AddAnchor(DX, DY: Integer);
    begin
      A.DX := DX;
      A.DY := DY;
      AnchorList.Add(A);
    end;

  begin
    AnchorList.Clear;
    for JX := 0 to Pred(Map.SizeX) do
      for JY := 0 to Pred(Map.SizeY) do
        if Map.Map[JX, JY] = mtUndefined then
        begin
          A.X := JX;
          A.Y := JY;
          AddAnchor(-1, 0);
          AddAnchor(+1, 0);
          AddAnchor( 0,-1);
          AddAnchor( 0,+1);
        end;
  end;

  procedure PositionShip(const AShip: TShip);
  var
    A: TAnchorPoint;
  begin
    repeat
      if AnchorList.Count = 0 then
      begin
        WriteLnLog('Cannot position ship, retrying');
        Exit;
      end;
      A := AnchorList[Rnd.Random(AnchorList.Count)];
      AnchorList.Remove(A);
      AShip.X := A.X;
      AShip.Y := A.Y;
      AShip.DX := A.DX;
      AShip.DY := A.DY;
    until Map.CanPutShipHere(AShip);
  end;

  procedure PositionMines;
  var
    NMines: Integer;
    FreeTiles: TMovesList;
    M, Tile: Integer;
  begin
    if (Actress <> nil) then
    begin
      NMines := -1;
      if Actress.HasQuirk('Miner') then
        NMines := Actress.QuirkValueInteger('Miner');
      if Actress.HasQuirk('Explosive') then
        NMines := 0;
      if NMines >= 0 then
      begin
        { Note, that as the ships are marked at this point, it's safe to ask MissAI for possible mine places
          It's not a valid approach to add a mine runtime }
        FreeTiles := MissAi(Map);
        if NMines = 0 then
          NMines := Round(FreeTiles.Count * 0.4);
        if NMines > FreeTiles.Count then
          NMines := FreeTiles.Count;
        for M := 1 to NMines do
        begin
          Tile := Rnd.Random(FreeTiles.Count);
          Map.Map[FreeTiles[Tile].X, FreeTiles[Tile].Y] := mtMine;
          FreeTiles.Delete(Tile);
        end;
        FreeTiles.Free;
      end;
    end;
  end;

var
  S: TShip;
  IX, IY: Integer;
begin
  AnchorList := TAnchorList.Create;
  repeat
    Map.Clear;

    for S in Ships.Ships do
    begin
      InitAnchorList;
      PositionShip(S);
      if AnchorList.Count = 0 then
        Break;
      Map.WriteShipToMap(S);
      S.Init(S.Tier);
      if S.Layer <> nil then
        S.Layer.IsActive := true;
    end;
  until AnchorList.Count > 0;
  AnchorList.Free;

  PositionMines;

  for IX := 0 to Pred(Map.SizeX) do
    for IY := 0 to Pred(Map.SizeY) do
    begin
      if Map.Map[IX, IY] = mtEmpty then
        Map.Map[IX, IY] := mtUndefined;
      Map.Vis[IX, IY] := IsPlayer;
    end;
end;

constructor TSeaMapInterface.Create(AOwner: TComponent);
begin
  inherited;
  Texture := TDrawableImage.Create('castle-data:/ui/sea/sea.png', true);
  FullSize := true;
  FirstRender := true;
  Map := TSeaMap.Create;
end;

destructor TSeaMapInterface.Destroy;
begin
  Texture.Free;
  Map.Free;
  inherited;
end;

procedure TSeaMapInterface.Render;
const
  TILE_MINE: TFloatRectangle = (Left: 0; Bottom: 1050; Width: 150; Height: 150);
  TILE_MINE_HIT: TFloatRectangle = (Left: 0; Bottom: 900; Width: 150; Height: 150);
  TILE_GLITCH: TFloatRectangle = (Left: 0; Bottom: 750; Width: 150; Height: 150);
  TILE_UNDEFINED: TFloatRectangle = (Left: 0; Bottom: 600; Width: 150; Height: 150);
  TILE_MISS: TFloatRectangle = (Left: 0; Bottom: 450; Width: 150; Height: 150);
  TILE_EMPTY: TFloatRectangle = (Left: 0; Bottom: 300; Width: 150; Height: 150);
  TILE_HIT: TFloatRectangle = (Left: 0; Bottom: 0; Width: 150; Height: 150);
  TILE_SHIP: TFloatRectangle = (Left: 0; Bottom: 150; Width: 150; Height: 150);
var
  IX, IY: Integer;
  ScrRects, ImgRects: array of TFloatRectangle;
  Count: Integer;
begin
  inherited; //empty
  //if FirstRender then // in case Player rescales widow
  begin
    FirstRender := false;

    // We are in screen coordinates here!
    if RenderRect.Width / Map.SizeX > RenderRect.Height / Map.SizeY then
      Scale := RenderRect.Height / Map.SizeY
    else
      Scale := RenderRect.Width / Map.SizeX;

    StartX := RenderRect.Left + (RenderRect.Width - Map.SizeX * Scale) / 2;
    StartY := RenderRect.Bottom + (RenderRect.Height - Map.SizeY * Scale) / 2;
  end;

  SetLength(ScrRects, Map.SizeX * Map.SizeY);
  SetLength(ImgRects, Map.SizeX * Map.SizeY);
  Count := 0;
  for IX := 0 to Pred(Map.SizeX) do
    for IY := 0 to Pred(Map.SizeY) do
    begin
      ScrRects[Count] := FloatRectangle(StartX + IX * Scale, StartY + IY * Scale, Scale, Scale);
      if Map.GlitchMap[IX, IY] then
        ImgRects[Count] := TILE_GLITCH
      else
      if Map.Vis[IX, IY] then
      begin
        case Map.Map[IX, IY] of
          mtUndefined: ImgRects[Count] := TILE_UNDEFINED;
          mtMiss: ImgRects[Count] := TILE_MISS;
          mtEmpty: ImgRects[Count] := TILE_EMPTY;
          mtShipHit: ImgRects[Count] := TILE_HIT;
          mtShipAlive: ImgRects[Count] := TILE_SHIP;
          mtMine: ImgRects[Count] := TILE_MINE;
          mtMineHit: ImgRects[Count] := TILE_MINE_HIT;
          else
            raise Exception.Create('Unexpected Map value: ' + MapTileToString(Map.Map[IX, IY]));
        end
      end else
        ImgRects[Count] := TILE_UNDEFINED;
      Inc(Count);
    end;
  Texture.Draw(@ScrRects[0], @ImgRects[0], Count);
end;

function TSeaMapInterface.IsPlayer: Boolean;
begin
  Result := Actress = nil;
end;

procedure TSeaMapInterface.AddMine;
var
  PossibleMines: TMovesList;
  M: TMove;
begin
  PossibleMines := GetPossibleMinesPlaces(Map);
  if PossibleMines.Count > 0 then
  begin
    M := PossibleMines[Rnd.Random(PossibleMines.Count)];
    Map.WriteMapSafe(M.X, M.Y, mtMine);
    PossibleMines.Free;
  end else
    raise Exception.Create('Cannot add a mine, there are no places on the map');
end;
procedure TSeaMapInterface.RevealLargestShip;
begin
  Map.MarkShip(Ships.LargestShip);
end;

procedure TSeaMapInterface.ProcessShipKilled(const AShip: TShip);
begin
  if Assigned(OnShipDestroyed) then
    OnShipDestroyed;
  Map.MarkShip(AShip);
  if (AShip.Tier >= MinimalLargeShipTier) and Ships.NoLargeTierShipsAlive then
  begin
    WriteLnLog('All large ships destroyed');
  end;
  if Ships.AllShipsDestroyed then
  begin
    WriteLnLog('All ships destroyed');
    if not IsPlayer then
      StateMain.PlayerVictory
    else
      StateMain.PlayerDefeat;
  end;
  if (Ships.LastShip <> nil) then
  begin
    if IsPlayer then
      SoundLastPlayerShipWarning
    else
    if Actress.HasQuirk('Submissive') then
    begin
      Actress.RevealQuirk('Submissive');
      Map.MarkShip(Ships.LastShip);
    end;
  end;
end;

procedure TSeaMapInterface.KillShip(const AShip: TShip);
begin
  Map.MarkShipKilled(AShip); //TODO: it's not optimal here for sure
  AShip.Kill;
  ProcessShipKilled(AShip);
end;

function TSeaMapInterface.ShotHere(const Mx, My: Integer): Boolean;
var
  J: Integer;

  procedure UpdateGlitchingPattern;
  var
    GlitchChance: Single;
  begin
    if (Actress <> nil) and Actress.HasQuirk('Glitchy') then
    begin
      Actress.RevealQuirk('Glitchy');
      GlitchChance := 0.2;
      Actress.Poses[pNormal].GetCoverStatus;
      if not Actress.Poses[pNormal].BottomCovered then
        GlitchChance += 0.2;
      if not Actress.Poses[pNormal].TopCovered then
        GlitchChance += 0.2;
      if Ships.NoLargeTierShipsAlive then
        GlitchChance += 0.1;
      Map.ApplyGlitch(GlitchChance);
    end;
  end;

begin
  if not Map.CanShootHere(Mx, My) then
    Exit(false);

  Result := true;

  if (not IsPlayer) then //TODO TEMPORARY----------------------------------------------
    SoundShot;
  Map.Vis[Mx, My] := true;
  case Map.Map[Mx, My] of
    mtUndefined: begin
         if (not IsPlayer) then //TODO TEMPORARY---------------------------------------
           SoundMiss;
         Map.Map[Mx, My] := mtMiss;
       end;
    mtShipAlive: Map.Map[Mx, My] := mtShipHit;
    mtMine:
      begin
        Map.Map[Mx, My] := mtMineHit;
        //Mine explosion sound TODO
        OnMineHit;
        if Actress <> nil then
        begin
          if Actress.HasQuirk('Miner') then
            Actress.RevealQuirk('Miner')
          else
          if Actress.HasQuirk('Explosive') then
            Actress.RevealQuirk('Explosive')
          else
            raise Exception.Create('Piratess has a mine, but is neither Miner nor Explosive!');
        end;
      end;
    else
      raise Exception.Create('Unexpected Map value: ' + MapTileToString(Map.Map[Mx, My]));
  end;
  for J := 0 to Pred(Ships.Ships.Count) do
    if Ships.Ships[J].IsHere(Mx, My) then
    begin
      Ships.Ships[J].HitHere(Mx, My);
      if (not IsPlayer) then //TEMPORARY -----------------------------------------
        SoundHit;
      {$ifdef ShipsCannotTouchCorners}
      Map.WriteMapSafe(Mx + 1, My + 1);
      Map.WriteMapSafe(Mx - 1, My + 1);
      Map.WriteMapSafe(Mx + 1, My - 1);
      Map.WriteMapSafe(Mx - 1, My - 1);
      {$endif}
      if not Ships.Ships[J].IsAlive then
        ProcessShipKilled(Ships.Ships[J]);
    end;

  UpdateGlitchingPattern;
end;

function TSeaMapInterface.Press(const Event: TInputPressRelease): Boolean;
var
  Mx, My: Integer;
begin
  Result := inherited;
  if Result then
    Exit;
  if FirstRender then // variables aren't ready yet
    Exit;

  if Event.EventType = itMouseButton then
  begin
    // note: StartX and Scale are in _Screen_ coordinates
    Mx := Trunc((Event.Position.x - StartX) / Scale);
    My := Trunc((Event.Position.y - StartY) / Scale);
    if (Mx >=0) and (MX < Map.SizeX) and (My >= 0) and (My < Map.SizeY) then
      if Assigned(OnClick) then
        OnClick(Mx, My);
  end;
end;

end.

